package dao;

import model.User;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;
import utils.C3P0Util;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * 对用户实体的底层数据库操作
 */
public class UserDAO {
    /**
     * 查找用户是否存在
     *
     * @param account       用户邮箱
     * @param user_password 用户密码
     * @return 返回用户
     */
    public User userLogin(String account, String user_password, String action) {
        User user = null;
        QueryRunner queryRunner = new QueryRunner(C3P0Util.getDataSource());
        String sql = "";
        if (action.equals("mail")) {//根据邮箱查询
            sql = "select * from user where user_mail=? and user_password=?";
        } else if (action.equals("name")) {//根据用户名查询
            sql = "select * from user where user_name=? and user_password=?";
        }
        Object params[] = {account, user_password};
        try {
            user = (User) queryRunner.query(sql, new BeanHandler(User.class), params);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return user;
    }

    /**
     * 增加用户的操作
     *
     * @param user_mail     用户邮箱
     * @param user_password 用户密码
     * @param user_name     用户名字
     * @return 返回用户
     */
    public User addUser(String user_mail, String user_password, String user_name) {
        int row = 0;
        QueryRunner queryRunner = new QueryRunner(C3P0Util.getDataSource());
        String sql = "INSERT INTO user (user_mail, user_name, user_password, user_experience) VALUES (?,?,?,0)";
        Object params[] = {user_mail, user_name, user_password};
        try {
            row = queryRunner.update(sql, params);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        User user = null;
        if (row > 0) {
            user = new User();
            //新建用户的各项初始化
            user.setUser_experience(0);
            user.setUser_mail(user_mail);
            user.setUser_password(user_password);
            user.setUser_name(user_name);
            byte b = 0;
            user.setUser_vertify(b);
        }
        return user;
    }

    /**
     * 查找是否有该用户名或者邮箱的用户
     *
     * @param name_or_mail 用户名或者邮箱
     * @param action       查询类型checkMail，checkName
     * @return true表示有
     */
    public boolean haveUser(String name_or_mail, String action) {
        Long ret = null;
        int count = 0;
        QueryRunner queryRunner = new QueryRunner(C3P0Util.getDataSource());
        String field = null;
        String sql = null;
        //根据动作类型
        switch (action) {
            case "checkMail":
                field = "user_mail";
                sql = "select count(*) from user where user_mail=?";
                break;
            case "checkName":
                field = "user_name";
                sql = "select count(*) from user where user_name=?";
                break;
            default:
                return true;
        }

        Object params[] = {name_or_mail};
        try {
            ret = (Long) queryRunner.query(sql, new ScalarHandler(), params);
            count = ret.intValue();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return count > 0;
    }

    /**
     * 更新用户密码的操作
     *
     * @param user_name     用户名
     * @param user_password 用户密码
     * @param newpassword   新用户密码
     * @return 返回true表示成功
     */
    public boolean updateUserPassword(String user_name, String user_password, String newpassword) {
        int row = 0;
        QueryRunner queryRunner = new QueryRunner(C3P0Util.getDataSource());
        String sql = "UPDATE user SET user_password = ? WHERE user_name = ? and user_password = ?";
        Object params[] = {newpassword, user_name, user_password};
        try {
            row = queryRunner.update(sql, params);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return row > 0;
    }

    /**
     * 设置用户已经验证邮箱
     *
     * @param username 用户名
     */
    public void setUserVertify(String username) {
        int row = 0;
        QueryRunner queryRunner = new QueryRunner(C3P0Util.getDataSource());
        String sql = "UPDATE user SET user_vertify =1  WHERE user_name = ?";
        Object params[] = {username};
        try {
            row = queryRunner.update(sql, params);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * 设置用户的头像路径
     *
     * @param username 用户名
     */
    public void setUserFace(String username, String picname) {
        int row = 0;
        QueryRunner queryRunner = new QueryRunner(C3P0Util.getDataSource());
        String sql = "UPDATE user SET face_url =? WHERE user_name = ?";
        Object params[] = {picname, username};
        try {
            row = queryRunner.update(sql, params);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * 根据用户名获得用户
     *
     * @param username 用户名
     * @return 返回最新数据的用户
     */
    public User getUserByName(String username, Connection coon) {
        User user = null;
        QueryRunner queryRunner = new QueryRunner(C3P0Util.getDataSource());
        String sql = "";
        sql = "select * from user where user_name=?";
        Object params[] = {username};
        try {
            if (coon != null) {
                user = (User) queryRunner.query(coon, sql, new BeanHandler(User.class), params);
            } else {
                user = (User) queryRunner.query(sql, new BeanHandler(User.class), params);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return user;
    }

    public User getUserByName(String username) {
        return getUserByName(username, null);
    }
}
