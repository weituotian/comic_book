package dao;

import model.Image;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import utils.C3P0Util;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * 对漫画的图片的底层数据库操作
 */
public class ImageDAO {
    /**
     * 获得某章节下的所有图片链接
     *
     * @param chapterID 章节id
     */
    public List<Image> getChapterImages(int chapterID) {
        List<Image> imageList = new ArrayList<Image>();
        QueryRunner runner = new QueryRunner(C3P0Util.getDataSource());
        String sql = "select * from image where chapter_id=?";
        Object[] params = {chapterID};
        try {
            imageList = (List<Image>) runner.query(sql, new BeanListHandler(Image.class), params);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return imageList;
    }
}
