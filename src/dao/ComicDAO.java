package dao;

import model.Comic;
import model.Comic_s;
import model.Tag;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import utils.C3P0Util;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * 漫画的底层操作.
 */
public class ComicDAO {
    /**
     * 根据漫画id获得该漫画
     *
     * @param comicId 漫画id
     * @return 返回漫画
     */
    public Comic getComicDetail(int comicId, Connection coon) {
        Comic comic = null;
        QueryRunner qr = new QueryRunner(C3P0Util.getDataSource());
        String sql = "select * from comic where id=?";
        Object[] params = {comicId};
        try {
            if (coon != null) {
                comic = (Comic) qr.query(coon, sql, new BeanHandler(Comic.class), params);
            } else {
                comic = (Comic) qr.query(sql, new BeanHandler(Comic.class), params);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return comic;
    }

    public Comic getComicDetail(int comicId) {
        return getComicDetail(comicId, null);
    }

    /**
     * 获得比较轻量的漫画id
     *
     * @param comicId 漫画id
     * @param coon    连接
     * @return Comic_s
     */
    public Comic_s getComic_sDetail(int comicId, Connection coon) {
        Comic_s comic = null;
        QueryRunner qr = new QueryRunner(C3P0Util.getDataSource());
        String sql = "select * from comic where id=?";
        Object[] params = {comicId};
        try {
            if (coon != null) {
                comic = (Comic_s) qr.query(coon, sql, new BeanHandler(Comic_s.class), params);
            } else {
                comic = (Comic_s) qr.query(sql, new BeanHandler(Comic_s.class), params);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return comic;
    }

    /**
     * 获得漫画list
     *
     * @return
     */
    public List<Comic_s> getComicList() {
        //随机抽取
        //SELECT * FROM comic WHERE id>= ((SELECT MAX(id) FROM comic)-(SELECT MIN(id) FROM comic)) * RAND() + (SELECT MIN(id) FROM comic) LIMIT 1
        //
        List<Comic_s> list = new ArrayList<Comic_s>();
        QueryRunner qr = new QueryRunner(C3P0Util.getDataSource());
        String sql = "SELECT * FROM comic order by lastUpdateTime limit 21";
        try {
            list = (List<Comic_s>) qr.query(sql, new BeanListHandler(Comic_s.class));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    /**
     * 获得一部漫画的所有标签
     *
     * @return list
     */
    public List<Tag> getComicTags(int comicId) {
        String sql = "select tag.* from comic_tag,tag where comic_id=? and tag_id=tag.id";
        Object[] params = {comicId};
        return DAO_common.beanListQuery(sql, Tag.class, params);
    }

    /**
     * 增加漫画的点击量
     *
     * @param comicId 漫画id
     */
    public void addComicClick(int comicId) {
        String sql = "update comic set click=click+1 where id=?";
        Object[] params = {comicId};
        DAO_common.updateQuery(sql, params);
    }

}
