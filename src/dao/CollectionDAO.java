package dao;

import model.Collection;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import utils.C3P0Util;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

/**
 * 收藏的底层操作
 */
public class CollectionDAO {
    /**
     * 添加收藏
     *
     * @param userName 用户名字
     * @param comicId  漫画id
     * @return true表示成功插入
     */
    public boolean addCollection(String userName, int comicId) {
        int row = 0;
        QueryRunner qr = new QueryRunner(C3P0Util.getDataSource());

        Timestamp times = new Timestamp(System.currentTimeMillis());
        Object[] params = {userName, comicId, times};

        String sql = "insert into collection(user_name,comic_id,time) values(?,?,?);";

        try {
            row = qr.update(sql, params);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return row > 0;
    }

    /**
     * 删除用户的一个收藏
     *
     * @param userName 用户名
     * @param comicId  漫画id
     * @return true表示成功
     */
    public boolean deleteCollection(String userName, int comicId) {
        int row = 0;
        QueryRunner qr = new QueryRunner(C3P0Util.getDataSource());
        Object[] params = {userName, comicId};
        String sql = "delete from collection where user_name=? and comic_id=?";
        try {
            row = qr.update(sql, params);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return row > 0;
    }

    /**
     * 删除用户的多个收藏
     *
     * @param userName 用户名
     * @param comicIds 漫画id数组
     * @return
     */
    public boolean deleteMultiCollection(String userName, String comicIds) {
        int row = 0;
        QueryRunner qr = new QueryRunner(C3P0Util.getDataSource());
        Object[] params = {userName};
        String sql = "delete from collection where user_name=? and comic_id in " + comicIds;
        try {
            row = qr.update(sql, params);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return row > 0;
    }

    /**
     * 检查用户是否已经收藏过该漫画了
     *
     * @param userName 用户名
     * @param comicId  漫画id
     * @return true表示已经收藏
     */
    public boolean isUserHaveCollection(String userName, int comicId, Connection coon) {
        QueryRunner qr = new QueryRunner(C3P0Util.getDataSource());
        Object[] params = {userName, comicId};
        String sql = "select count(*) from collection where user_name=? and comic_id=?";
        return DAO_common.ScalarQuery(coon, qr, sql, params) > 0;
    }


    /**
     * 返回一个用户所有的收藏
     *
     * @param userName 用户名
     * @return list
     */
    public List<Collection> getUserAllCollection(String userName, Connection coon) {
        List<Collection> list = null;
        QueryRunner qr = new QueryRunner(C3P0Util.getDataSource());
        Object[] params = {userName};
        String sql = "select * from collection where user_name=?";
        try {
            if (coon != null) {
                list = (List<Collection>) qr.query(coon, sql, new BeanListHandler(Collection.class), params);
            } else {
                list = (List<Collection>) qr.query(sql, new BeanListHandler(Collection.class), params);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    /**
     * 获得某漫画的收藏数量
     *
     * @param comicId 漫画id
     * @param coon    连接
     * @return 数量
     */
    public int getCollectionCount(int comicId, Connection coon) {
        QueryRunner qr = new QueryRunner(C3P0Util.getDataSource());
        Object[] params = {comicId};
        String sql = "select count(*) from collection where comic_id=?";
        return DAO_common.ScalarQuery(coon, qr, sql, params);
    }
}
