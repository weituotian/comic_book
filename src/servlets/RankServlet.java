package servlets;

import model.Rank;
import net.sf.json.JSONArray;
import services.RankServices;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

/**
 * 获取排行的json接口
 */
@WebServlet(name = "RankServlet", urlPatterns = "/rank")
public class RankServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter pw = response.getWriter();
        RankServices rankServices = new RankServices();
        List<Rank> list = rankServices.getRank();
        pw.write(JSONArray.fromObject(list).toString());
    }
}
