package servlets;

import model.Collection;
import model.User;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import services.CollectionServices;
import utils.MyJSONUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

/**
 * 收藏的view接口
 */
@WebServlet(name = "CollectionServlet", urlPatterns = "/collect")
public class CollectionServlet extends HttpServlet {


    public CollectionServlet() {
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String type = req.getParameter("type");
        switch (type) {
            case "add":
                process(req, resp, type);
                break;
            case "delete":
                process(req, resp, type);
                break;
            case "delete_multi":
                delete_all(req, resp);
                break;
            case "list":
                getlist(req, resp);
                return;
            default:
        }

    }

    /**
     * 根据结果写jsonobject
     *
     * @param result    是否成功
     * @param succHint  成功提示
     * @param errorHint 失败提示
     * @return jsonobject
     */
    private JSONObject writeMsg(boolean result, String succHint, String errorHint) {
        JSONObject json = new JSONObject();
        if (result) {
            json.put("result", true);
            json.put("msg", succHint);
        } else {
            json.put("result", false);
            json.put("msg", errorHint);
        }
        return json;
    }

    private void delete_all(HttpServletRequest req, HttpServletResponse resp) {
        String userName = req.getParameter("user_name");
        String comicId = req.getParameter("comic_id");
        CollectionServices cs = new CollectionServices();

        boolean result = false;
        result = cs.deleteMultiCollection(userName, comicId);//删除多个收藏

        try {
            PrintWriter pw = resp.getWriter();

            JSONObject jsonObject = writeMsg(result, "删除成功！", "操作失败！");

            pw.write(jsonObject.toString());

            cs.finish();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void process(HttpServletRequest req, HttpServletResponse resp, String type) {
        String userName = req.getParameter("user_name");
        String comicId = req.getParameter("comic_id");

        boolean result = false;
        String succHint = "", errorHint = "";

        CollectionServices cs = new CollectionServices();
        switch (type) {
            case "add":
                result = cs.addCollection(userName, comicId);
                succHint = "收藏成功！";
                errorHint = "收藏失败！";
                break;
            case "delete":
                result = cs.deleteCollection(userName, comicId);
                succHint = "取消收藏成功！";
                errorHint = "取消收藏失败！";
                break;
            default:
                return;
        }

        //更新后的收藏数量
        int count = cs.getCollectionCount(comicId);

        try {
            PrintWriter pw = resp.getWriter();
            JSONObject json = writeMsg(result, succHint, errorHint);

            json.put("count", count);

            pw.write(json.toString());

            cs.finish();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void getlist(HttpServletRequest req, HttpServletResponse resp) {
        CollectionServices collectionServices = new CollectionServices();
        User user = (User) req.getSession().getAttribute("user");
        List<Collection> list = collectionServices.getUserAllCollection(user.getUser_name());

        try {
            PrintWriter pw = resp.getWriter();
            pw.write(JSONArray.fromObject(list, MyJSONUtils.jsonConfig).toString());
        } catch (IOException e) {
            e.printStackTrace();
        }

        collectionServices.finish();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
