package servlets;

import model.User;
import net.sf.json.JSONObject;
import services.UserServices;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * 暂时只有修改密码
 */
@WebServlet(name = "PersonalServlet", urlPatterns = "/person")
public class PersonalServlet extends HttpServlet {

    //处理个人业务，修改密码
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter pw = response.getWriter();
        JSONObject result = new JSONObject();
        //获取session中的user
        User user = (User) request.getSession().getAttribute("user");
        if (user == null) {
            result.put("error", 1);
            result.put("msg", "你还没有登录呢！");
            pw.write(result.toString());
            return;
        }
        //获取参数
        String oldpwd = request.getParameter("oldpwd");
        String newpwd = request.getParameter("newpwd");
        if (oldpwd == null || newpwd == null || oldpwd.equals("") || newpwd.equals("")) {
            result.put("error", 2);
            result.put("msg", "数据有问题！");
            pw.write(result.toString());
            return;
        }
        //新建用户服务
        UserServices userServices = new UserServices();
        if (userServices.changeUserPassword(user.getUser_name(), oldpwd, newpwd)) {
            result.put("error", 0);
            result.put("msg", "修改密码成功！");
        } else {
            result.put("error", 3);
            result.put("msg", "操作失败！");
        }
        pw.write(result.toString());
        user.getUser_name();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
