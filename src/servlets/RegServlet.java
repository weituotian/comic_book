package servlets;

import model.User;
import net.sf.json.JSONObject;
import services.MailServices;
import services.UserServices;
import utils.MyJSONUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * 提到到这个servlet处理注册或者检查用户名是否重复
 */
@WebServlet(name = "RegServlet", urlPatterns = "/reg")
public class RegServlet extends HttpServlet {

    /**
     * 真正的注册操作
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //网页的输出流
        PrintWriter printWriter = response.getWriter();
        //返回对象json初始化
        JSONObject result = new JSONObject();
        //获取参数
        String reg_email = request.getParameter("reg_email");
        String reg_name = request.getParameter("reg_name");
        String reg_password = request.getParameter("reg_password");
        if (reg_email == null || reg_name == null || reg_password == null) {
            result.put("error", 1);
            result.put("msg", "提交信息有误！");
            printWriter.write(result.toString());
            return;
        }
        //用户服务初始化
        UserServices userServices = new UserServices();
        User user = userServices.addUser(reg_email, reg_password, reg_name);
        //注册成功，返回的user不为空
        if (user != null) {
            HttpSession session = request.getSession();
            session.setAttribute("user", user);//同时保留登录信息
            session.setMaxInactiveInterval(3600);
            if (session.isNew()) {
                System.out.println("session.isNew");
            }
            //同时发送一下邮箱
            MailServices mailServices = new MailServices(request.getContextPath(), user.getUser_name());
            try {
                mailServices.sendVertifyCode(user.getUser_mail());
            } catch (Exception e) {
                e.printStackTrace();
            }
            //
            result.put("error", 0);
            result.put("msg", "注册成功！");
        } else {
            result.put("error", 2);
            result.put("msg", "注册失败！");
        }

        printWriter.write(MyJSONUtils.jsonObjectToString(result));
    }

    /**
     * 检测用户名和邮箱是否已经存在
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     **/
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //网页的输出流
        PrintWriter printWriter = response.getWriter();
        //返回对象json初始化
        JSONObject result = new JSONObject();

        String action = request.getParameter("action");
        String toCheckStr = request.getParameter("toCheckStr");
        if (toCheckStr == null || toCheckStr.equals("")) {
            result.put("canReg", false);//返回《不能操作》
            printWriter.write(result.toString());
            return;
        }

        //创建服务
        UserServices userServices = new UserServices();
        switch (action) {
            case "checkMail":
                System.out.println("str:" + toCheckStr);
                result.put("canReg", !userServices.haveUserEmail(toCheckStr));//已经有该邮箱返回《不能操作》
                break;
            case "checkName":
                result.put("canReg", !userServices.haveUserName(toCheckStr));//已经有该用户名返回《不能操作》
                break;
            default:
                return;
        }

        printWriter.write(result.toString());
    }
}
