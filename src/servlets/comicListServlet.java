package servlets;

import net.sf.json.JSONArray;
import services.ComicServices;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * json接口
 * 专门获取各种list
 */
@WebServlet(name = "comicListServlet", urlPatterns = "/clist")
public class comicListServlet extends HttpServlet {

    private ComicServices cs;

    public comicListServlet() {
        cs = new ComicServices();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter pw = response.getWriter();
        JSONArray result = JSONArray.fromObject(cs.getComicList());
        pw.write(result.toString());
    }
}
