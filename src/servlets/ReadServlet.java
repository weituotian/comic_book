package servlets;

import model.Chapter;
import model.Image;
import model.User;
import net.sf.json.JSONArray;
import services.ChapterServices;
import services.HistoryServices;
import services.ImageServices;
import utils.MyJSONUtils;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

/**
 * 阅读漫画的view，在转到相应的jsp
 */
@WebServlet(name = "ReadServlet", urlPatterns = "/read")
public class ReadServlet extends HttpServlet {

    public ReadServlet() {
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter pw = response.getWriter();

        String chapterid = request.getParameter("chapterid");

        //request.setAttribute("thisid", chapterid);

        RequestDispatcher rd = null;
        if (chapterid != null && !chapterid.equals("")) {
            //新建图片服务
            ImageServices imageServices = new ImageServices();
            //新建章节服务
            ChapterServices chapterServices = new ChapterServices();
            //获得图片list
            List<Image> images = imageServices.getChapterImages(Integer.parseInt(chapterid));
            if (images.size() >= 0) {
                //确认到有该章节了
                JSONArray result = JSONArray.fromObject(images);
                //获得下一章节
                List<Chapter> chapters = chapterServices.getNearChapter(chapterid);
                JSONArray next = JSONArray.fromObject(chapters, MyJSONUtils.jsonConfig);

                //当前章节
                Chapter thisChapter = chapterServices.getChapterDetail(chapterid);
                request.setAttribute("thisChapter", thisChapter);
//                for (Chapter chapter : chapters) {
//                    if (chapter.getId() == Integer.parseInt(chapterid)) {
//                        thisChapter = chapterServices.getChapterDetail(chapterid);
//                    }
//                }

                //保存浏览记录
                User user = (User) request.getSession().getAttribute("user");
                if (user != null) {
                    //用户已经登录
                    HistoryServices historyServices = new HistoryServices();
                    if (thisChapter != null) {
                        //添加历史
                        historyServices.addHistory(user.getUser_name(), thisChapter.getComic_id(), Integer.parseInt(chapterid));
                    }
                }

                //保存在请求当中,并且转到read.jsp
                request.setAttribute("images", result);
                request.setAttribute("chapters", next);
                rd = request.getRequestDispatcher("/read.jsp");
                rd.forward(request, response);
            }
        } else {
            pw.write("no params!");
        }
    }
}
