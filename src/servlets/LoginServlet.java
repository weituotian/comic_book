package servlets;

import model.User;
import net.sf.json.JSONObject;
import services.UserServices;
import utils.MyJSONUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * 登录操作提交到这里处理
 */
@WebServlet(name = "loginServlet", urlPatterns = "/login")
public class LoginServlet extends HttpServlet {

    /**
     * 退出操作
     *
     * @param request
     * @param response
     * @throws IOException
     */
    private void doExit(HttpServletRequest request, HttpServletResponse response) throws IOException {
        HttpSession session = request.getSession();
        if (session != null) {
            session.invalidate();
            response.getWriter().write("登出成功！");
        }
    }

    /**
     * 登录操作
     *
     * @param request
     * @param response
     * @throws IOException
     */
    private void doLogin(HttpServletRequest request, HttpServletResponse response) throws IOException {
        //网页的输出流
        PrintWriter printWriter = response.getWriter();
        //用户服务初始化
        UserServices userServices = new UserServices();
        //返回对象json初始化
        JSONObject result = new JSONObject();
        //获取参数
        String account = request.getParameter("account");
        String user_password = request.getParameter("password");

        if (account == null || user_password == null) {
            result.put("error", 1);
            result.put("msg", "用户名或者密码为空！");
            String ret = MyJSONUtils.jsonObjectToString(result);
            printWriter.write(ret);
            return;
        }

        User user = userServices.userLogin(account, user_password);
        if (user != null) {//可以登录
            result.put("error", "0");
            result.put("msg", "登录成功！");
            HttpSession session = request.getSession();
            session.setAttribute("user", user);
            //session.setAttribute("user_name",user.getUser_name());
            //session.setAttribute("user_mail",user.getUser_mail());
            session.setMaxInactiveInterval(3600);
        } else {
            result.put("error", "2");
            result.put("msg", "email或者密码错误！");
        }

        //写出json的内容
        String ret = MyJSONUtils.jsonObjectToString(result);
        printWriter.write(ret);
    }


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //获取参数
        String action = request.getParameter("act");

        //判断操作类型
        if (action != null) {
            if (action.equals("exit")) {
                doExit(request, response);
            } else if (action.equals("login")) {
                doLogin(request, response);
            }
        }
    }

}
