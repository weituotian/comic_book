package servlets;

import services.CommentServices;
import services.ReplyServices;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 增加评论和回复的servlet
 */
@WebServlet(name = "addCommentServlet", urlPatterns = "/addcomment")
public class addCommentServlet extends HttpServlet {

    private void addComment(String user_name, String comic_id, String content) {
        if (user_name != null && comic_id != null && content != null) {
            //新建服务
            CommentServices coms = new CommentServices();
            coms.addComment(user_name, comic_id, content);
        }
    }

    private void addReply(String user_name, String comment_id, String content) {
        if (user_name != null && comment_id != null && content != null) {
            //新建服务
            ReplyServices rps = new ReplyServices();
            rps.addReply(user_name, content, comment_id);
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String user_name = request.getParameter("username");//用户名
        String content = request.getParameter("content");//内容

        String comic_id = request.getParameter("comic");//漫画id
        String comment_id = request.getParameter("commentid");//评论id

        String type = request.getParameter("type");//操作类型
        switch (type) {
            case "reply":
                addReply(user_name, comment_id, content);
                break;
            case "comment":
                addComment(user_name, comic_id, content);
                break;
            default:

                break;
        }

    }
}
