package servlets;

import model.User;
import services.MailServices;
import services.UserServices;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * 邮箱验证提交到这里处理
 */
@WebServlet(name = "MailServlet", urlPatterns = "/mail")
public class MailServlet extends HttpServlet {

    /**
     * 发送邮件
     *
     * @param request
     * @param response
     * @throws IOException
     */
    private void sendMail(HttpServletRequest request, HttpServletResponse response) throws IOException {
        PrintWriter pw = response.getWriter();
        //检测登录
        User user = (User) request.getSession().getAttribute("user");
        if (user == null) {
            String path = request.getContextPath() + "/login.jsp";
            pw.write("请<a href=\"" + path + "\">登录</a>后发送验证邮箱！");
            return;
        }
        MailServices mailServices = new MailServices(request.getRequestURL().toString(), user.getUser_name());
        try {
            mailServices.sendVertifyCode(user.getUser_mail());
        } catch (Exception e) {
            e.printStackTrace();
        }
        String path = request.getContextPath() + "/index.jsp";
        pw.write("邮件成功被发送，返回<a href=\"" + path + "\">首页</a>！");
    }

    /**
     * 验证邮箱验证码
     *
     * @param request
     * @param response
     * @throws IOException
     */
    private void checkCode(HttpServletRequest request, HttpServletResponse response) throws IOException {
        PrintWriter pw = response.getWriter();
        String code = request.getParameter("code");
        System.out.println("code:" + code);
        //检测登录
        User user = (User) request.getSession().getAttribute("user");
        if (user == null) {
            String path = request.getContextPath() + "/login.jsp";
            pw.write("请<a href=\"" + path + "\">登录</a>后验证！");
            return;
        }
        if (code == null || code.equals("")) {
            pw.write("不合法的访问哦！");
            return;
        }

        //url解密
        //code= URLDecoder.decode(code,"utf-8");

        //验证开始
        boolean flag = false;
        MailServices mailServices = new MailServices(request.getRequestURL().toString(), user.getUser_name());
        try {
            flag = mailServices.checkMailCode(code);
        } catch (Exception e) {
            e.printStackTrace();
        }
        String path = request.getContextPath() + "/home/main.jsp";
        if (flag) {
            //成功
            pw.write(String.format("邮箱验证成功，返回<a href='%s'>个人中心</a>", path));
            //写入数据库
            UserServices userServices = new UserServices();
            userServices.setUserVertify(user.getUser_name());
        } else {
            pw.write(String.format("邮箱验证失败，返回<a href='%s'>个人中心</a>", path));
        }

    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("act");
        if (action != null) {
            //根据动作类型来执行不同的动作
            if (action.equals("send")) {
                sendMail(request, response);
            } else if (action.equals("check")) {
                checkCode(request, response);
            }
        }

    }
}
