package servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 小小用来测试的servlet
 */
@WebServlet(name = "TestServlet", urlPatterns = "/test")
public class TestServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("--------------------begin------------------------");
        System.out.println("request.getContextPath():" + request.getContextPath());
        System.out.println("request.getRequestURI():" + request.getRequestURI());
        System.out.println("request.getLocalName():" + request.getLocalName());
        System.out.println("request.getLocalAddr():" + request.getLocalAddr());
        System.out.println("request.getServletPath():" + request.getServletPath());
        System.out.println("request.getRequestURL():" + request.getRequestURL());
        System.out.println("request.getServerName():" + request.getServerName());
        System.out.println("request.getRemoteAddr():" + request.getRemoteAddr());
        System.out.println("request.getPathInfo():" + request.getPathInfo());
        System.out.println("request.getRemoteHost():" + request.getRemoteHost());
        System.out.println("request.getRemoteAddr():" + request.getRemoteAddr());
        System.out.println("getServletContext().getRealPath():" + getServletContext().getRealPath("/"));
        System.out.println("getServletContext().getContextPath():" + getServletContext().getContextPath());
        System.out.println("------------------end--------------------------");
    }
}
