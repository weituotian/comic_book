package servlets;

import model.Page;
import model.Reply;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import services.CommentServices;
import services.ReplyServices;
import utils.MyJSONUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

/**
 * 评论的json接口
 * http://localhost:8080/comment?comic=1&page=1
 */
@WebServlet(name = "CommentServlet", urlPatterns = "/comment")
public class CommentServlet extends HttpServlet {

    private CommentServices commentServices;
    private ReplyServices replyServices;

    public CommentServlet() {
        commentServices = new CommentServices();
        replyServices = new ReplyServices();
    }


    /**
     * 返回某一页评论list
     *
     * @param request
     * @param response
     * @throws IOException
     */
    private void getlist(HttpServletRequest request, HttpServletResponse response) throws IOException {
        PrintWriter pw = response.getWriter();

        String comicId = request.getParameter("comic");
        String page = request.getParameter("page");

        Page page1 = new Page(Integer.parseInt(page));

        JSONArray result = commentServices.getComicComments(Integer.parseInt(comicId), page1);

        pw.write(result.toString());
    }

    /**
     * 返回评论的页数
     *
     * @param request
     * @param response
     * @throws IOException
     */
    private void getCount(HttpServletRequest request, HttpServletResponse response) throws IOException {
        PrintWriter pw = response.getWriter();

        String comicId = request.getParameter("comic");

        Page page1 = commentServices.getCommentPage(comicId);
        JSONObject result = JSONObject.fromObject(page1);

        pw.write(result.toString());

    }

    /**
     * 获得某一页的回复
     */
    public void getReply(HttpServletRequest request, HttpServletResponse response) throws IOException {
        PrintWriter pw = response.getWriter();

        String commentId = request.getParameter("cid");
        String page = request.getParameter("page");

        List<Reply> replyList = replyServices.getPageReplys(commentId, page);

        pw.write(JSONArray.fromObject(replyList, MyJSONUtils.jsonConfig).toString());
    }

    /**
     * 获得第一页的回复
     *
     * @param request
     * @param response
     * @throws IOException
     */
    public void getPageOneReply(HttpServletRequest request, HttpServletResponse response) throws IOException {
        PrintWriter pw = response.getWriter();

        String commentId = request.getParameter("cid");

        JSONArray result = replyServices.getReplysWithPage(commentId);
        pw.write(result.toString());
    }


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String type = request.getParameter("type");
        switch (type) {
            case "count":
                getCount(request, response);
                break;
            case "list":
                getlist(request, response);
                break;
            case "replys":
                getReply(request, response);
                break;
            case "refreshReply":
                getPageOneReply(request, response);
                break;
        }

    }
}
