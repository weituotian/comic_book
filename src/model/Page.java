package model;

/**
 * 分页的辅助类
 */
public class Page {
    private int current;//当前页面
    private int pageSize = 10;//每一页显示多少条
    //private int start;//数据库中开始查询的语句
    private int total;//总记录数
    private int pagaTotal;//总共多少页

    public Page() {
        this.current = 0;
    }

    public Page(int current) {
        this.current = current;
    }

    public int getPagaTotal() {
        return pagaTotal;
    }

    public void setPagaTotal(int pagaTotal) {
        this.pagaTotal = pagaTotal;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
        pagaTotal = (total / pageSize) + ((total % pageSize) == 0 ? 0 : 1);
//        if (pagaTotal==0){
//            //这种情况是当0条记录时，才返回0页，如果只有1条记录，上面也会产生1
//            pagaTotal=1;
//        }
    }

    public int getCurrent() {
        return current;
    }

    public void setCurrent(int current) {
        this.current = current;
    }

    public int getStart() {
        return (current - 1) * pageSize;
    }

    public Page setPageSize(int pageSize) {
        this.pageSize = pageSize;
        return this;
    }

    public int getPageSize() {
        return pageSize;
    }

}
