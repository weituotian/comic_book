package model;

/**
 * 每一张漫画图片.
 */
public class Image {
    //图片id
    private int id;
    //图片的地址
    private String address;
    //图片所属的章节id
    private String chapter_id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getChapter_id() {
        return chapter_id;
    }

    public void setChapter_id(String chapter_id) {
        this.chapter_id = chapter_id;
    }
}
