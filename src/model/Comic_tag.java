package model;

/**
 * 存储漫画有哪些标签的实体
 */
public class Comic_tag {
    private int comic_id;//漫画id
    private int tag_id;//标签id

    public int getComic_id() {
        return comic_id;
    }

    public void setComic_id(int comic_id) {
        this.comic_id = comic_id;
    }

    public int getTag_id() {
        return tag_id;
    }

    public void setTag_id(int tag_id) {
        this.tag_id = tag_id;
    }
}
