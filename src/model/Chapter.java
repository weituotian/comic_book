package model;

import java.io.Serializable;
import java.sql.Date;

/**
 * 一个漫画的一个章节
 */
public class Chapter implements Serializable {
    //章节id
    private int id;
    //该章节在漫画中的序号
    private int comic_index;
    //属于哪个漫画
    private int comic_id;
    //该章节更新时间
    private Date updatetime;
    //该章节的标题
    private String title;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getComic_index() {
        return comic_index;
    }

    public void setComic_index(int comic_index) {
        this.comic_index = comic_index;
    }

    public int getComic_id() {
        return comic_id;
    }

    public void setComic_id(int comic_id) {
        this.comic_id = comic_id;
    }

    public Date getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(Date updatetime) {
        this.updatetime = updatetime;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
