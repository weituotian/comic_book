package model;

/**
 * Created by ange on 2016/4/21.
 * http://www.cnblogs.com/xdp-gacl/p/3902537.html
 */
public class User {
    //变量名与数据库相同
    //用户名
    private String user_name;
    //用户密码
    private String user_password;
    //用户邮箱
    private String user_mail;
    //用户经验值
    private int user_experience;
    //是否验证邮箱
    private byte user_vertify;
    //头像的名字
    private String face_url;

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUser_password() {
        return user_password;
    }

    public void setUser_password(String user_password) {
        this.user_password = user_password;
    }

    public String getUser_mail() {
        return user_mail;
    }

    public void setUser_mail(String user_mail) {
        this.user_mail = user_mail;
    }

    public int getUser_experience() {
        return user_experience;
    }

    public void setUser_experience(int user_experience) {
        this.user_experience = user_experience;
    }

    public byte getUser_vertify() {
        return user_vertify;
    }

    public void setUser_vertify(byte user_vertify) {
        this.user_vertify = user_vertify;
    }

    public String getFace_url() {
        return face_url;
    }

    public void setFace_url(String face_url) {
        this.face_url = face_url;
    }
}
