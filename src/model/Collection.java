package model;

import java.sql.Date;

/**
 * 收藏
 */
public class Collection {
    //用户id
    private String user_name;
    //漫画id
    private int comic_id;
    //收藏时间
    private Date time;
    //关联的漫画实体，数据库中不存在该字段
    private Comic_s comic;

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public int getComic_id() {
        return comic_id;
    }

    public void setComic_id(int comic_id) {
        this.comic_id = comic_id;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public Comic_s getComic() {
        return comic;
    }

    public void setComic(Comic_s comic) {
        this.comic = comic;
    }
}
