package model;

import java.sql.Date;

/**
 * 一个漫画的实体
 */
public class Comic {
    //漫画id
    private int id;
    //漫画名字
    private String name;
    //漫画作者
    private String author;
    //章节数目
    private int chapterNum;
    //连载状态
    private byte state;
    //封面
    private String cover;
    //简介
    private String description;
    //漫画的点击量
    private int click;
    //最近更新的章节id
    private int lastUpdateChapterId;
    //最近更新的章节时间
    private Date lastUpdateTime;
    //最近更新的章节名字
    private String lastUpdateChapterName;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public int getChapterNum() {
        return chapterNum;
    }

    public void setChapterNum(int chapterNum) {
        this.chapterNum = chapterNum;
    }

    public byte getState() {
        return state;
    }

    public void setState(byte state) {
        this.state = state;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getClick() {
        return click;
    }

    public void setClick(int click) {
        this.click = click;
    }

    public int getLastUpdateChapterId() {
        return lastUpdateChapterId;
    }

    public void setLastUpdateChapterId(int lastUpdateChapterId) {
        this.lastUpdateChapterId = lastUpdateChapterId;
    }

    public Date getLastUpdateTime() {
        return lastUpdateTime;
    }

    public void setLastUpdateTime(Date lastUpdateTime) {
        this.lastUpdateTime = lastUpdateTime;
    }

    public String getLastUpdateChapterName() {
        return lastUpdateChapterName;
    }

    public void setLastUpdateChapterName(String lastUpdateChapterName) {
        this.lastUpdateChapterName = lastUpdateChapterName;
    }
}
