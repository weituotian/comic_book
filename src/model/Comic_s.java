package model;

/**
 * 小型的漫画信息实体，用于被装在list中
 */
public class Comic_s {
    //漫画id
    private int id;
    //漫画名字
    private String name;
    //封面
    private String cover;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
