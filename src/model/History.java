package model;

import java.sql.Date;

/**
 * 观看历史
 */
public class History {
    //用户名
    private String user_name;
    //漫画id
    private int comic_id;
    //时间
    private Date time;
    //阅读到第几章
    private int chapter_id;
    //所看漫画[数据库中无该字段]
    private Comic_s comic;
    //所看章节【数据库中无该字段】
    private Chapter chapter;

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public int getComic_id() {
        return comic_id;
    }

    public void setComic_id(int comic_id) {
        this.comic_id = comic_id;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public int getChapter_id() {
        return chapter_id;
    }

    public void setChapter_id(int chapter_id) {
        this.chapter_id = chapter_id;
    }

    public Comic_s getComic() {
        return comic;
    }

    public void setComic(Comic_s comic) {
        this.comic = comic;
    }

    public Chapter getChapter() {
        return chapter;
    }

    public void setChapter(Chapter chapter) {
        this.chapter = chapter;
    }
}
