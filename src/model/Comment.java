package model;

import java.io.Serializable;
import java.sql.Date;
import java.util.List;

/**
 * 评论实体
 */
public class Comment implements Serializable {
    //评论id
    private int id;
    //哪个用户发的
    private String user_name;
    //在哪个漫画
    private int comic_id;
    //内容
    private String content;
    //发布时间
    private Date time;

    //回复分页的实体
    private Page replyPage;
    //该评论的所有回复实体[数据库不存在该字段]
    private List<Reply> replys;
    //评论的用户实体[数据库不存在该字段]
    private User user;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public int getComic_id() {
        return comic_id;
    }

    public void setComic_id(int comic_id) {
        this.comic_id = comic_id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public Page getReplyPage() {
        return replyPage;
    }

    public void setReplyPage(Page replyPage) {
        this.replyPage = replyPage;
    }

    public List<Reply> getReplys() {
        return replys;
    }

    public void setReplys(List<Reply> replys) {
        this.replys = replys;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
