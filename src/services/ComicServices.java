package services;

import dao.ComicDAO;
import model.Comic;
import model.Comic_s;
import model.Tag;

import java.util.List;

/**
 * 提供漫画的服务
 */
public class ComicServices {

    private ComicDAO comicDAO;

    public ComicServices() {
        comicDAO = new ComicDAO();
    }

    /**
     * 根据漫画id获得该漫画
     *
     * @param comicId 漫画id
     * @return 返回漫画
     */
    public Comic getComicDetail(int comicId) {
        return comicDAO.getComicDetail(comicId);
    }

    /**
     * 获得漫画list
     *
     * @return list
     */
    public List<Comic_s> getComicList() {
        return comicDAO.getComicList();
    }

    /**
     * 获得漫画的tag
     *
     * @param comicId 漫画id
     * @return String
     */
    public String getComicTags(String comicId) {
        List<Tag> list = comicDAO.getComicTags(Integer.parseInt(comicId));
        String str = "";
        for (Tag eachTag : list) {
            str += eachTag.getName() + "/";
        }
        str = str.substring(0, str.length() - 1);//去掉最后一个"/"
        return str;
    }

    /**
     * 增加漫画的点击量
     *
     * @param comicId 漫画id
     */
    public void addComicClick(String comicId) {
        comicDAO.addComicClick(Integer.parseInt(comicId));
    }
}
