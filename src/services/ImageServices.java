package services;

import dao.ImageDAO;
import model.Image;

import java.util.List;

/**
 * 提供图片相关的服务
 */
public class ImageServices {
    private ImageDAO imageDAO;

    public ImageServices() {
        this.imageDAO = new ImageDAO();
    }

    /**
     * 提供获得某章节下所有图片的服务
     *
     * @param chapterId 章节id
     * @return 图片list
     */
    public List<Image> getChapterImages(int chapterId) {
        return imageDAO.getChapterImages(chapterId);
    }
}
