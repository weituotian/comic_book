package services;

import dao.RankDAO;
import model.Rank;

import java.util.List;

/**
 * 提供排名操作的业务
 */
public class RankServices {
    private RankDAO rankDAO;

    public RankServices() {
        rankDAO = new RankDAO();
    }

    /**
     * 获得排行list
     *
     * @return list
     */
    public List<Rank> getRank() {
        return rankDAO.getRank();
    }

    /**
     * 更新排行榜
     */
    public void updateRank() {
        rankDAO.updateRank();
    }
}
