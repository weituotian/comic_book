package services;

import dao.ChapterDAO;
import model.Chapter;

import java.util.List;

/**
 * 提供章节相关的服务
 */
public class ChapterServices {
    private ChapterDAO chapterDAO;

    public ChapterServices() {
        this.chapterDAO = new ChapterDAO();
    }

    /**
     * 根据漫画id获得该漫画下的所有章节
     *
     * @param comic_id 漫画id
     * @return 章节list
     */
    public List<Chapter> getComicChapterList(String comic_id) {
        return chapterDAO.getComicChapterList(Integer.parseInt(comic_id));
    }

    /**
     * 根据章节id获得临近的章节
     *
     * @param chapterId 章节id
     * @return 下一章节
     */
    public List<Chapter> getNearChapter(String chapterId) {
        return chapterDAO.getNearChapter(Integer.parseInt(chapterId));
    }

    /**
     * 根据章节id获得该章节的详情
     *
     * @param chapterId 章节id
     * @return Chapter
     */
    public Chapter getChapterDetail(String chapterId) {
        return chapterDAO.getChapterDetail(Integer.parseInt(chapterId));
    }
}
