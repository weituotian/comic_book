package services;

import dao.UserDAO;
import model.User;
import utils.ValidateUtil;

/**
 * 提供用户操作的相关业务
 */
public class UserServices {
    private UserDAO userDAO;

    /**
     * 构造函数
     */
    public UserServices() {
        userDAO = new UserDAO();
    }

    /**
     * 提供验证用户密码服务
     *
     * @param account       用户帐号，可以是邮箱或者用户名
     * @param user_password 用户密码
     * @return 返回用户
     */
    public User userLogin(String account, String user_password) {
        if (ValidateUtil.checkEmail(account)) {
            //是邮箱
            return userDAO.userLogin(account, user_password, "mail");
        }
        return userDAO.userLogin(account, user_password, "name");
    }

    /**
     * 提供注册服务
     *
     * @param user_mail     用户邮箱
     * @param user_password 用户密码
     * @param user_name     用户名字
     * @return 返回用户
     */
    public User addUser(String user_mail, String user_password, String user_name) {
        return userDAO.addUser(user_mail, user_password, user_name);
    }

    /**
     * 提供查找是否有用户名的服务
     *
     * @param user_name 用户名
     * @return true表示返回有
     */
    public boolean haveUserName(String user_name) {
        return userDAO.haveUser(user_name, "checkName");
    }

    /**
     * 提供查找是否有邮箱的服务
     *
     * @param user_mail 用户邮箱
     * @return true表示返回有
     */
    public boolean haveUserEmail(String user_mail) {
        return userDAO.haveUser(user_mail, "checkMail");
    }

    /**
     * 提供更改用户密码的服务
     *
     * @param user_name     用户名
     * @param user_password 用户密码
     * @param newpassword   新用户密码
     * @return 返回true表示成功
     */
    public boolean changeUserPassword(String user_name, String user_password, String newpassword) {
        return userDAO.updateUserPassword(user_name, user_password, newpassword);
    }

    /**
     * 设置用户邮箱已经验证的服务
     *
     * @param username 用户名
     */
    public void setUserVertify(String username) {
        userDAO.setUserVertify(username);
    }

    /**
     * 设置用户上传的头像路径
     *
     * @param username 用户
     */
    public User setUserFace(String username, String picname) {
        userDAO.setUserFace(username, picname);
        return updateUserInfo(username);
    }

    /**
     * 更新用户session服务
     *
     * @param username 用户名
     * @return 返回更新后的用户
     */
    public User updateUserInfo(String username) {
        return userDAO.getUserByName(username);
    }
}
