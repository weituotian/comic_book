package utils;

/**
 * Created by ange on 2016/4/21.
 */

import com.mchange.v2.c3p0.ComboPooledDataSource;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * 数据库工具类
 * 文档：
 * http://commons.apache.org/proper/commons-dbutils/apidocs/index.html
 *
 * @author liang
 */
public class C3P0Util {
    static ComboPooledDataSource cpds = null;

    static {
        //这里有个优点，写好配置文件，想换数据库，简单
        //cpds = new ComboPooledDataSource("oracle");//这是oracle数据库
        cpds = new ComboPooledDataSource("mysql");//这是mysql数据库
    }

    /**
     * 获得数据库连接
     *
     * @return Connection
     */
    public static Connection getConnection() {
        try {
            return cpds.getConnection();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static DataSource getDataSource() {//连接池，数据源
        return cpds;
    }

}

