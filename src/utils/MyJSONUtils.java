package utils;

import net.sf.json.JSONArray;
import net.sf.json.JSONException;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;
import net.sf.json.processors.JsonValueProcessor;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * 处理json与对象之间关系的工具类
 * Created by ange on 2016/4/23.
 * http://blog.chinaunix.net/uid-20577907-id-2876165.html
 * https://www.youtube.com/watch?v=q4IhM4GBhF8
 */
public class MyJSONUtils {
    /**
     * @author JSON工具类
     */

    //处理date类型的转化
    public static JsonConfig jsonConfig = new JsonConfig();

    static {
        jsonConfig.registerJsonValueProcessor(java.sql.Date.class, new JsonValueProcessor() {
            private SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

            public Object processObjectValue(String key, Object value, JsonConfig jsonConfig) {
                return value == null ? "" : sd.format(value);
            }

            public Object processArrayValue(Object value, JsonConfig jsonConfig) {
                return null;
            }
        });
    }

    /***
     * 将List对象序列化为JSON文本
     */
    public static <T> String toJSONString(List<T> list) {
        JSONArray jsonArray = JSONArray.fromObject(list);

        return jsonArray.toString();
    }

    /***
     * 将对象序列化为JSON文本
     *
     * @param object
     * @return
     */
    public static String toJSONString(Object object) {
        JSONArray jsonArray = JSONArray.fromObject(object);

        return jsonArray.toString();
    }

    /***
     * 将JSON对象数组序列化为JSON文本
     *
     * @param jsonArray
     * @return
     */
    public static String toJSONString(JSONArray jsonArray) {
        return jsonArray.toString();
    }

    /***
     * 将JSON对象序列化为JSON文本
     *
     * @param jsonObject
     * @return
     */
    public static String toJSONString(JSONObject jsonObject) {
        return jsonObject.toString();
    }

    public static String jsonObjectToString(JSONObject result) {
//        JSONArray jsonArray=new JSONArray();
//        jsonArray.add(result);
        return result.toString();
    }

    /**
     * 将Json对象转换成Map
     *
     * @param jsonObject json对象
     * @return Map对象
     * @throws JSONException
     */
    public static Map toMap(JSONObject jsonObject) throws JSONException {

        Map result = new HashMap();
        Iterator iterator = jsonObject.keys();
        String key = null;
        String value = null;

        while (iterator.hasNext()) {

            key = (String) iterator.next();
            value = jsonObject.getString(key);
            result.put(key, value);

        }
        return result;

    }
}
