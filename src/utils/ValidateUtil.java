package utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 提供各种验证
 */
public class ValidateUtil {
    /**
     * 验证邮箱
     *
     * @param email 邮箱
     * @return 正确返回true
     */
    public static boolean checkEmail(String email) {
        boolean flag = false;
        try {
            String check = "^[\\w]+@[\\w]+\\.[\\w]+$";
            Pattern regex = Pattern.compile(check);
            Matcher matcher = regex.matcher(email);
            flag = matcher.matches();
        } catch (Exception e) {
            flag = false;
        }
        return flag;
    }

}
