package task;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import services.RankServices;

import java.util.TimerTask;

/**
 * 定时更新排行榜
 */

public class RankingTask extends TimerTask {
    private static Log log = LogFactory.getLog(RankingTask.class);
    private static boolean isRunning = false;

    private RankServices rankServices;

    public RankingTask() {
        rankServices = new RankServices();
    }

    public void run() {
        rankServices.updateRank();
        //log.debug("开始执行任务..."); //开始任务
        System.out.println("执行更新排行榜任务...");
//        if (!isRunning) {
//            isRunning = true;
//            log.debug("开始执行任务..."); //开始任务
//            //working  add what you want to do
//            log.debug("执行任务完成..."); //任务完成
//            isRunning = false;
//        } else {
//            log.debug("上一次任务执行还未结束..."); //上一次任务执行还未结束
//        }
    }
}
