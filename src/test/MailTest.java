package test;

import com.sun.mail.util.MailSSLSocketFactory;
import org.junit.Before;

import javax.mail.*;
import javax.mail.Message.RecipientType;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.security.GeneralSecurityException;
import java.util.Properties;

/**
 * Created by ange on 2016/4/26.
 */
public class MailTest {

    @Before
    public void setUp() throws Exception {

    }

    @org.junit.Test
    public void beginTest() throws MessagingException, GeneralSecurityException {
//        String smtp = "smtp.qq.com";
//        String from = "921977939@qq.com";
//        String to = "weituotian@163.com";
//        String copyto = "抄送人";
//        String subject = "邮件主题";
//        String content = "邮件内容";
//        String username="921977939@qq.com";
//        String password="ewpganonwaokbebh";
//        //String filename = "附件路径，如：F:\\笔记<a>\\struts2</a>与mvc.txt";
//        boolean flag=Mail.sendAndCc(smtp, from, to, copyto, subject, content, username, password);
//        System.out.print("flag:"+flag);

        // 配置发送邮件的环境属性
        final Properties props = new Properties();

        // 开启debug调试
        props.setProperty("mail.debug", "true");

        MailSSLSocketFactory sf = new MailSSLSocketFactory();
        sf.setTrustAllHosts(true);
        props.put("mail.smtp.ssl.enable", "true");
        props.put("mail.smtp.ssl.socketFactory", sf);

        // 表示SMTP发送邮件，需要进行身份验证
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.host", "smtp.qq.com");
        // 发件人的账号
        props.put("mail.user", "921977939@qq.com");
        // 访问SMTP服务时需要提供的密码
        props.put("mail.password", "ewpganonwaokbebh");

        // 构建授权信息，用于进行SMTP进行身份验证
        Authenticator authenticator = new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                // 用户名、密码
                String userName = props.getProperty("mail.user");
                String password = props.getProperty("mail.password");
                return new PasswordAuthentication(userName, password);
            }
        };
        // 使用环境属性和授权信息，创建邮件会话
        Session mailSession = Session.getInstance(props, authenticator);
        // 创建邮件消息
        MimeMessage message = new MimeMessage(mailSession);
        // 设置发件人
        InternetAddress form = new InternetAddress(
                props.getProperty("mail.user"));
        message.setFrom(form);

        // 设置收件人
        InternetAddress to = new InternetAddress("921977939@qq.com");
        message.setRecipient(RecipientType.TO, to);

        // 设置抄送
        InternetAddress cc = new InternetAddress("921977939@qq.com");
        message.setRecipient(RecipientType.CC, cc);

        // 设置密送，其他的收件人不能看到密送的邮件地址
        InternetAddress bcc = new InternetAddress("weituotian@163.com");
        message.setRecipient(RecipientType.CC, bcc);

        // 设置邮件标题
        message.setSubject("测试邮件");

        // 设置邮件的内容体
        message.setContent("<a href='http://www.fkjava.org'>测试的HTML邮件</a>", "text/html;charset=UTF-8");

        // 发送邮件
        Transport.send(message);
    }
}