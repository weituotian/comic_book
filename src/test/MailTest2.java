package test;

import com.sun.mail.util.MailSSLSocketFactory;
import org.junit.Before;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.security.GeneralSecurityException;
import java.util.Properties;


public class MailTest2 {

    @Before
    public void setUp() throws Exception {

    }

    @org.junit.Test
    public void beginTest() throws MessagingException, GeneralSecurityException {
        Properties props = new Properties();

        // 开启debug调试
        props.setProperty("mail.debug", "true");
        // 发送服务器需要身份验证
        props.setProperty("mail.smtp.auth", "true");
        // 设置邮件服务器主机名
        props.setProperty("mail.host", "smtp.qq.com");
        // 发送邮件协议名称
        props.setProperty("mail.transport.protocol", "smtp");

        MailSSLSocketFactory sf = new MailSSLSocketFactory();
        sf.setTrustAllHosts(true);
        props.put("mail.smtp.ssl.enable", "true");
        props.put("mail.smtp.ssl.socketFactory", sf);

        Session session = Session.getInstance(props);

        Message msg = new MimeMessage(session);

        //邮件标题
        msg.setSubject("注册验证码");

        String s = "我是超人";

        //邮件正文
        //msg.setText(s);
        msg.setContent("<a href='11'>测试的HTML邮件</a>", "text/html;charset=UTF-8");
        msg.setFrom(new InternetAddress("921977939@qq.com"));

        Transport transport = session.getTransport();
        transport.connect("smtp.qq.com", "921977939@qq.com", "ewpganonwaokbebh");

        transport.sendMessage(msg, new Address[]{new InternetAddress("weituotian@163.com"), new InternetAddress("921977939@qq.com")});
        transport.close();

    }

}