package test;


/**
 * Created by ange on 2016/4/26.
 */

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class pathTest {
    @org.junit.Before
    public void setUp() throws Exception {
    }

    @org.junit.Test
    public void testPath() {
        String check = "^http://(.+?)/.*$";
        //check="(.*)(\\d+)(.*)";
        Pattern regex = Pattern.compile(check);
        String test = "http://localhost:8080/home/main.jsp";
        Matcher m = regex.matcher(test);
        //boolean flag = m.matches();
        //System.out.println("flag:"+flag);
        System.out.println("m.find():" + m.find());
        //System.out.println("m.group(0):"+m.group(0));
        System.out.println("m.group(1):" + m.group(1));

        // 按指定模式在字符串查找
        String line = "This order was placed for QT3000! OK?";
        line = "http://localhost:8080/home/main.jsp";
        String pattern = "^http://(.+?)/.*$";

        // 创建 Pattern 对象
        Pattern r = Pattern.compile(pattern);

        // 现在创建 matcher 对象
        Matcher matcher = r.matcher(line);
        if (matcher.find()) {
            System.out.println("Found value: " + matcher.group(0));
            System.out.println("Found value: " + matcher.group(1));
            System.out.println("Found value: " + matcher.group(2));
        } else {
            System.out.println("NO MATCH");
        }
    }
}
