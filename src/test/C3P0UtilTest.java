package test;

import model.User;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import utils.C3P0Util;

import java.sql.SQLException;

/**
 * C3P0连接池的测试
 */
public class C3P0UtilTest {
    private C3P0Util c3P0Util;

    @org.junit.Before
    public void setUp() throws Exception {
        c3P0Util = new C3P0Util();
    }

    @org.junit.Test
    public void testGetConnection() throws Exception {
        System.out.println("" + C3P0Util.getConnection());
        String user_name = "李超人";
        String user_password = "123456";
        User user = null;
        QueryRunner queryRunner = new QueryRunner(C3P0Util.getDataSource());
        String sql = "select * from user where user_name=? and user_password=?";
        Object params[] = {user_name, user_password};
        Object params2[] = {};
        try {
            user = (User) queryRunner.query(sql, new BeanHandler(User.class), params);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if (user == null) {
            System.out.println("user==null");
        } else {
            System.out.println("user!=null");
            System.out.println("user:" + user.getUser_name());
        }
        //插入测试
/*        String sql2="INSERT INTO user (user_name, user_password, user_mail, user_experience) VALUES (?, '123456', '123456', '123456')";
        Object params3[]={"神人"};
        queryRunner.update(sql2,params3);*/
    }
}