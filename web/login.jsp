<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="include/top_container.jsp" %>

<%--登录页面jsp--%>


<div class="row-fluid">
    <div class="span12">
        <form id="login_form" class="form-horizontal">
            <div class="control-group">
                <label class="control-label" for="login_account">帐号</label>
                <div class="controls">
                    <input id="login_account" name="login_account" type="text" placeholder="邮箱或者用户名">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="login_password">密码</label>
                <div class="controls">
                    <input id="login_password" name="login_password" type="password">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="login_vali">验证码：</label>
                <div class="controls">
                    <input id="login_vali" name="login_vali" type="text" value=""/>
                </div>
                <div class="controls">
                    <img src="/valicode" width="200px" height="50px" id="valicodeBox"/>
                </div>
            </div>
            <div class="control-group">
                <div class="controls">
                    <label class="checkbox">
                        <input type="checkbox">
                        Remember me</label>
                    <button id="login_submit" type="submit" class="btn">登陆</button>
                </div>
            </div>
        </form>
    </div>
</div>


<script type="text/javascript">
    $(function () {
        var login_submit = $('#login_submit');
        var login_password = $('#login_password');
        var login_account = $('#login_account');
        var login_form = $('#login_form');
        var myDialog = $('#dialog-message');

        //验证码相关
        var login_vali = $('#login_vali');
        var valicodeBox = $('#valicodeBox');
        login_vali.blur(function () {
            login_vali.valid();
        });

        //检查登录
        if (isLogin()) {
            //已经登录
            login_form.hide();
            myDialog.find('#msg').html("你已经登录了哦");
            //打开对话框
            myDialog.dialog("open");
        } else {
            //没有登录
        }


        //登录按钮动作
        login_submit.click(function () {
            login_form.submit();
        });

        //自动完成
        login_account.autocomplete({
            delay: 0,
            autoFocus: true,
            source: function (request, response) {
                var hosts = ['qq.com', '163.com', '263.com', 'sina.com.cn', 'gmail.com', 'hotmail.com'],
                        term = request.term,		//获取用户输入的内容
                        name = term,				//邮箱的用户名
                        host = '',					//邮箱的域名
                        ix = term.indexOf('@'),		//@的位置
                        result = [];				//最终呈现的邮箱列表
                result.push(term);
                //当有@的时候，重新分别用户名和域名
                if (ix > -1) {
                    name = term.slice(0, ix);
                    host = term.slice(ix + 1);
                }
                if (name) {
                    //如果用户已经输入@和后面的域名，
                    //那么就找到相关的域名提示，比如bnbbs@1，就提示bnbbs@163.com
                    //如果用户还没有输入@或后面的域名，
                    //那么就把所有的域名都提示出来
                    var findedHosts = (host ? $.grep(hosts, function (value, index) {
                        return value.indexOf(host) > -1
                    }) : hosts);
                    var findedResult = $.map(findedHosts, function (value, index) {
                        return name + '@' + value;
                    });
                    //数组连接
                    result = result.concat(findedResult);
                }
                response(result);
            }
        });

        //验证
        login_form.validate({
            //键盘弹起触发的函数
            onkeyup: function (element) {
                var index = ["login_vali"];
                if ($.inArray(element.name, index) == -1) {//不在这个数组中
                    $(element).valid();
                }
            },
            highlight: function (element, errorClass, validClass) {
                $(element).addClass("error").removeClass("valid");
                $(element).parent("div").find("span.ui-icon").addClass("ui-icon-circle-close").removeClass("ui-icon-circle-check");
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).removeClass("error").addClass("valid");
            },
            //提交按钮被点击后执行的函数,需要验证成功
            submitHandler: function (form) {
                //暂时禁用提交按钮
                login_submit.attr("disabled", true);
                login_submit.text("提交中……");
                toReg();
            },
            rules: {
                login_account: {
                    required: true,
                    rangelength: [5, 20]
                },
                login_password: {
                    required: true,
                    rangelength: [5, 20]
                }
            },
            messages: {
                login_vali: {
                    required: "填写验证码",
                    remote: "验证码不正确！"
                },
                login_account: {
                    required: "帐号为空",
                    rangelength: $.format("长度为{0}到{1}")
                },
                login_password: {
                    required: "密码为空",
                    rangelength: $.format("长度为{0}到{1}")
                }
            },
            //设置错误信息存放标签
            errorElement: "em",
            //放置错误标签的位置
            errorPlacement: function (error, element) {
                element.after(error);
                error.before($("<span class='ui-icon' style='display: inline-block'></span>"));
                $(element).valid();//触发验证
            },
            success: function (label, dom) {
                //字段验证成功后的方法
                label.parent("div").find("input").removeClass("error").addClass("valid");
                label.parent("div").find("span.ui-icon").addClass("ui-icon-circle-check").removeClass("ui-icon-circle-close");
                label.addClass("valid").text('ok');
            }
        });

        //添加验证规则
        login_vali.rules("add", {
            required: true,
            remote: {
                url: '/valicode',
                type: 'POST',
                data: {
                    code: function () {
                        return login_vali.val();
                    }
                },
                dataType: "json",
                dataFilter: function (data, type) {
                    var jsonObj = jQuery.parseJSON(data);
                    return jsonObj.canReg;
                }
            }
        });

        //验证码箱子被点击就刷新
        valicodeBox.click(function () {
            valicodeBox.attr("src", "/valicode?t=" + Math.random());
        });

        //去注册
        function toReg() {
            $.ajax({
                url: "/login",
                type: "POST",
                datatype: "json",
                timeout: 3000,
                data: {
                    act: "login",
                    account: login_account.val(),
                    password: login_password.val()
                },
                success: function (rdata, textStatus) {
                    var data = $.parseJSON(rdata);

                    if (data.error > 0) {
                        //有错误
                        myDialog.find("#icon").attr("class", "ui-icon ui-icon-circle-close");
                        login_submit.attr("disabled", false);
                        login_submit.text("重新登录");
                    } else {
                        //登录成功
                        myDialog.find("#icon").attr("class", "ui-icon ui-icon-circle-check");
                        //3秒后跳转到首页
                        setTimeout(function () {
                            //转到登录前页面
                            window.location.href = $.cookie('before_login');
                        }, 3000);
                        login_submit.text("登录成功！");
                    }

                    //设置消息
                    myDialog.find('#msg').html(data.msg);
                    //打开对话框
                    myDialog.dialog("open");
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert(XMLHttpRequest);
                    alert(textStatus);
                    alert(errorThrown);
                }
            })
        }
    })
</script>
</body>
</html>
