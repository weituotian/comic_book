<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>漫画网</title>
    <!-- 引入 Bootstrap -->
    <!-- 新 Bootstrap 核心 CSS 文件 -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap-combined.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/jquery-ui-1.10.3.custom.css">
    <!-- 可选的Bootstrap主题文件（一般不使用） -->
    <link rel="stylesheet" href="http://apps.bdimg.com/libs/bootstrap/3.3.0/css/bootstrap-theme.min.css">
    <!-- jQuery文件。务必在bootstrap.min.js 之前引入 -->
    <script src="${pageContext.request.contextPath}/js/jquery-1.9.1.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/jquery-ui-1.10.3.custom.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/jquery.validate.js"></script>
    <!-- 最新的 Bootstrap 核心 JavaScript 文件 -->
    <script src="http://apps.bdimg.com/libs/bootstrap/3.3.0/js/bootstrap.min.js"></script>
</head>

<body>
<div class="row-fluid">
    <div class="span12">
        <div class="container-fluid">
            <div class="row-fluid">
                <div class="span12">
                    <div class="navbar navbar-inverse">
                        <div class="navbar-inner">
                            <div class="container-fluid"><a class="btn btn-navbar"
                                                            data-target=".navbar-responsive-collapse"
                                                            data-toggle="collapse"></a> <a class="brand"
                                                                                           href="${pageContext.request.contextPath}/index.jsp">漫画网</a>
                                <div class="nav-collapse collapse navbar-responsive-collapse">
                                    <ul class="nav">
                                        <li class="active"><a href="${pageContext.request.contextPath}/index.jsp">主页</a>
                                        </li>
                                        <li><a href="#">分类</a></li>
                                        <li><a href="#">排行</a></li>
                                        <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">个人中心</a>
                                            <ul class="dropdown-menu">
                                                <li>
                                                    <a href="${pageContext.request.contextPath}/home/fav.jsp">收藏</a>
                                                </li>
                                                <li>
                                                    <a href="${pageContext.request.contextPath}/home/history.jsp">历史</a>
                                                </li>
                                                <li><a href="${pageContext.request.contextPath}/home/main.jsp">我的资料</a>
                                                </li>
                                                <li class="divider"></li>
                                            </ul>
                                        </li>
                                    </ul>
                                    <ul class="nav pull-right">
                                        <li>
                                            <form class="form-search">
                                                <input class="input-medium search-query" type="text" title="搜索内容"/>
                                                <button type="submit" class="btn">查找</button>
                                            </form>
                                        </li>
                                        <li>
                                            <img src="" id="img_face"
                                                 style="display: none;height:auto;width: 36px;margin-top: 4px;border-radius: 36px;box-shadow: 0 0 12px #7E7E7E;"/>
                                        </li>
                                        <li>
                                            <a id="nav_username" href="${pageContext.request.contextPath}/home/main.jsp"
                                               style="display: none"></a>
                                        </li>
                                        <li>
                                            <a id="login_out" style="display: none">退出</a>
                                        </li>
                                        <li>
                                            <button id="nav_reg" class="btn btn-defalut navbar-btn">
                                                <a href="${pageContext.request.contextPath}/reg.jsp">注册</a>
                                            </button>
                                        </li>
                                        <li>
                                            <button id="nav_login" class="btn btn-defalut navbar-btn"
                                                    style="margin-left:10px">
                                                <a href="${pageContext.request.contextPath}/login.jsp">登录</a>
                                            </button>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<%--模态对话框--%>
<div id="dialog-message" title="注册" style="display: none">
    <p>
        <span id="icon" class="ui-icon " style="float:left; margin:0 7px 50px 0;"></span>
        <span id="msg"></span>
    </p>
</div>

<%--cookies插件--%>
<script src="${pageContext.request.contextPath}/js/jquery.cookie.js"></script>
<%--配合cookies插件实现object到json的转换--%>
<script src="${pageContext.request.contextPath}/js/jquery.json.js"></script>
<%--提供历史操作的--%>
<script src="${pageContext.request.contextPath}/js/history.js"></script>
<script type="text/javascript">
    //全局用户名
    var g_user_name = "${sessionScope.user.user_name}";
    //全局头像地址
    var g_face_name = "${sessionScope.user.face_url}";
    var g_contextPath = '${pageContext.request.contextPath}';

    //全局函数，获得头像url
    function getFaceURL() {
        if (g_face_name != "") {
            return g_contextPath + "/upload/" + g_face_name + "?t=" + Math.random();//给图片地址拼接一个随机数,防止被缓存
        } else {
            return g_contextPath + "/images/default-face.jpg";
        }
    }

    //全局函数，检查是否登录
    function isLogin() {
        return g_user_name != "";
    }

    //全局对话框
    var myDialog = $('#dialog-message');
    function openDialog(message, positionEle) {
        //设置消息
        myDialog.find('#msg').html(message);
        //打开对话框
        myDialog.dialog("option", "position", {of: positionEle});
        myDialog.dialog("open");
    }

    /**
     * 全局函数 ajax请求
     * @param url
     * @param data
     * @param callback
     */
    function g_myAjax(url, data, callback) {
        $.ajax({
            url: url,
            type: "GET",
            dataType: "json",
            timeout: 3000,
            data: data,
            success: function (data, textStatus) {
                callback(data);
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert(XMLHttpRequest + textStatus + errorThrown);
            }
        });
    }

    $(function () {
        //导航条上的注册按钮，登录按钮和显示用户名的地方
        var nav_reg = $('#nav_reg');
        var nav_login = $('#nav_login');
        var nav_username = $('#nav_username');
        var login_out = $('#login_out');

        //头像显示
        var img_face = $('#img_face');
        img_face.attr("src", getFaceURL());
        img_face.click(function () {
            window.location.href = g_contextPath + "/home/main.jsp";//点击头像跳到主页
        });

        if (isLogin()) {
            //已经登录
            nav_reg.hide();
            nav_login.hide();
            nav_username.html(g_user_name);
            nav_username.show();
            img_face.show();
            login_out.show();
        } else {
            //没有登录
        }

        //初始化对话框
        myDialog.dialog({
            show: "slide",       //显示弹窗出现的效果，slide为滑动效果
            hide: "explode",     //显示窗口消失的效果，explode为爆炸效果
            resizable: false,    //设置是否可拉动弹窗的大小，默认为true
            autoOpen: false,
            modal: true,
            buttons: {
                "确定": function () {
                    $(this).dialog("close");
                }
            },
            close: function () {
                //对话框关闭执行的按钮
            }
        });

        //登出按钮被点击
        login_out.click(function () {
            $.ajax({
                url: "/login",
                type: "GET",
                datatype: "json",
                timeout: 3000,
                data: {
                    act: "exit"
                },
                success: function (rdata, textStatus) {
                    //登录成功
                    myDialog.find("#icon").attr("class", "ui-icon ui-icon-circle-check");
                    //3秒后跳转到首页
                    setTimeout(function () {
                        window.location.href = $.cookie('before_login');//g_contextPath + "/index.jsp";
                    }, 3000);
                    //设置消息
                    myDialog.find('#msg').html(rdata);
                    //打开对话框
                    myDialog.dialog("open");
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert(XMLHttpRequest);
                    alert(textStatus);
                    alert(errorThrown);
                }
            });
        });
    });
</script>

