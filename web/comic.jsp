<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="include/top_container.jsp" %>

<%--漫画详情的页面--%>

<%--作品简介style--%>
<style>

    body, button, input, select, textarea {
        font: 12px/22px "宋体";
    }

    html {
        overflow-y: scroll;
    }

    address, cite, dfn, em, i {
        font-style: normal;
    }

    h1, h2, h3, h4, h5, h6 {
        font-size: 12px;
        font-weight: normal;
    }

    a {
        color: #666;
        text-decoration: none;
        outline: none;
    }

    button, input, select, textarea {
        font-size: 100%;
    }

    fieldset, img {
        width: 211px;
        height: 277px;
        border: none;
    }

    ::selection {
        background-color: #67a414;
        color: #fff;
    }

    .comic_info .left {
        float: left;
        width: 900px;
        position: relative;
        z-index: 5;
        left: 20%;
    }

    .comic_info {
        height: 415px;
        padding: 0 8px 0 20px;
        margin-bottom: 30px;
    }

    .wrap {
        width: 1200px;
        margin: 0 auto;
        position: relative;
    }

    .blue {
        color: #379be9 !important;
    }

    .blue {
        color: #1f7abc;
    }

    .comic_info .left .sign {
        float: left;
        margin: 16px 0 0 20px;
        _display: inline;
        font: 28px/30px "Microsoft Yahei";
    }

    .fl {
        float: left;
    }

    .fl {
        float: left;
    }

    .comic_info h1 {
        font: 28px/32px "Microsoft Yahei", simhei, simsun;
        color: #333;
        position: relative;
    }

    .comic_info .left_tag {
        float: left;
        margin: 23px 0 0 10px;
        line-height: 21px;
        color: #999;
    }

    .comic_info .jb {
        background: none;
        padding-left: 24px;
        color: #999;
        float: right;
        margin: 20px 15px 0 0;
        font-family: simsun, serif;
        font-size: 12px;
    }

    .clear {
        background: none;
        border: 0;
        clear: both;
        display: block;
        font-size: 0px;
        margin: 0;
        padding: 0;
        overflow: hidden;
        visibility: hidden;
        width: 0;
        height: 0;
    }

    .comic_info .coverBox {
        width: 234px;
        float: left;
    }

    .comic_info .info {
        float: right;
    }

    .comic_info .left .info {
        float: left;
        width: 626px;
        margin-left: 14px;
    }

    .comic_info .left .info .top {
        font-size: 14px;
        color: #666;
        border-bottom: 1px dashed #999;
        padding: 0 0 3px 15px;
        width: 605px;
        line-height: 34px;
        _padding-bottom: 10px;
    }

    .comic_info .left .info .words {
        color: #999;
        height: 160px;
        overflow: hidden;
        position: relative;
        word-break: break-all;
        word-wrap: break-word;
        padding: 5px;
    }

    .comic_info .btn_wrap a {
        background: url("http://k.static.u17i.com/v4/www/images/comicinfo/cs_pic.png") no-repeat -44px -180px;
        float: left;
        height: 24px;
        line-height: 24px;
        margin: 0 26px 0;
    }

    .comic_info .left .info .btn_wrap a {
        float: left;
        width: 163px;
        height: 49px;
        border-bottom: 4px solid #d0252f;
        -webkit-border-radius: 3px;
        -moz-border-radius: 3px;
        border-radius: 3px;
        background: #ff444f;
        color: #fff;
        text-align: center;
    }

    .comic_info .left .info .btn_wrap span {
        display: block;
        font: 14px "Arial";
    }

    input {
        vertical-align: middle;
    }

    input {
        vertical-align: middle;
    }

    .comic_info .left .info .btn_wrap strong {
        display: block;
        font: 20px/32px "Microsoft Yahei";
        text-align: center;
    }

    .comic_info .left .info .btn_wrap .btn_stored {
        background: #41d0d6;
        border-color: #2eb0a2;
        position: relative;
    }

    .comic_info .left .info .btn_wrap .btn_continue span {
        font: 12px "宋体";
    }

    .comic_info .left .info .btn_wrap .btn_start em {
        background: url(http://k.static.u17i.com/v4/www/images/comicinfo/cs_pic.png) no-repeat 0 -180px;
        float: left;
        width: 35px;
        height: 35px;
        margin: 7px 9px 0 13px;
    }

    .comic_info .left .info .btn_wrap .btn_start {
        font-size: 22px;
        float: left;
        line-height: 48px;
    }

    .comic_info .left .info .words .btn {
        display: inline-block;
        width: 40px;
        height: 18px;
        vertical-align: middle;
        background-position: 0 -90px;
        position: absolute;
        right: 10px;
        bottom: 22px;
    }

    .comic_info .role_label a {
        float: left;
        background: #fff1c8;
        border: 1px solid #f6b235;
        padding: 1px 4px;
        margin-right: 8px;
        margin-bottom: 6px;
        position: relative;
        white-space: nowrap;
    }

    .comic_info .role_label a em {
        color: #999;
    }

    .comic_info .role_label a span {
        display: none;
        padding: 5px 10px;
        position: absolute;
        left: -1px;
        top: 25px;
        border: 1px solid #d5c6bb;
        background: #fffded;
    }

    .ico_male {
        background-position: -19px -80px;
        width: 12px;
        height: 13px;
        margin-right: 3px;
    }

    .comic_label a, .comic_label li span {
        background: #f6d75a no-repeat;
        height: 14px;
        padding: 0 5px 0 20px;
        line-height: 14px;
        color: #fff;
        float: left;
        margin-right: 4px;
        border: 1px solid #f6d75a;
        border-radius: 2px;
        margin-bottom: 10px;
    }

    .comic_label a {
        white-space: nowrap;
    }

    .comic_info .left .info .label .tab_scroll div {
        position: absolute;
        left: 0px;
        top: 0px;
        z-index: 2;
    }

    .comic_info .left .info .label .tab_scroll span {
        position: absolute;
        left: 0;
        top: 0;
        width: 64px;
        height: 28px;
        background: url(http://k.static.u17i.com/v4/www/images/comicinfo/cs_pic.png) no-repeat 0 -150px;
    }

    .comic_info .left .info .label .tab_scroll a {
        display: block;
        width: 62px;
        height: 22px;
        border: 1px solid #e2e2e2;
        background: #eaeaea;
        text-align: center;
        -webkit-transition: all .1s ease-in;
        -moz-transition: all .1s ease-in;
        -o-transition: all .1s ease-in;
        transition: all .1s ease-in;
    }

    .comic_info .left .info .label .tab_scroll .curr {
        background: none;
        border-color: transparent;
        _border: none;
        _width: 64px;
        _height: 24px;
        _line-height: 24px;
        color: #fff;
        text-decoration: none;
    }

    .comic_info .left .info .top .line1 {
        _padding: 10px 0 12px;
        white-space: nowrap;
    }

    .comic_info .left .info .top i {
        color: #ff444f;
        padding-right: 20px;
    }

    .comic_info .left .info .top .line2 span {
        padding-right: 20px;
    }

    .comic_info .left .info .top .more .btn {
        display: inline-block;
        width: 40px;
        height: 18px;
        vertical-align: middle;
        background-position: 0 -90px;
    }

    .comic_info .left .info .top .more .pop_box p {
        height: 29px;
        line-height: 29px;
        border-bottom: 1px dashed #bcbcbc;
        padding: 0 10px;
    }

    .comic_info .left .info .top .more .pop_box span {
        float: left;
        width: 120px;
        padding-right: 0;
    }

    .comic_info .left .info .top .more .pop_box em {
        color: #ff444f;
    }

    .comic_info .left .info .top .line2 b {
        background: url(http://k.static.u17i.com/v4/www/images/comicinfo/cs_pic.png) no-repeat 0 -135px;
        display: inline-block;
        width: 16px;
        height: 12px;
        vertical-align: middle;
        margin-left: 3px;
    }

    .comic_info .left .info .top .line1 span {
        color: #6dad15;
        padding-right: 20px;
    }

    .comic_info .cover {
        position: relative;
        background: url(http://k.static.u17i.com/v4/www/images/comicinfo/bg_cover.png) no-repeat;
        width: 224px;
        height: 289px;
        padding: 1px 0 0 10px;
        z-index: 3;
    }

    .comic_info .coverBox .btn_wrap {
        margin-top: 15px;
        margin-left: 48px;
    }

    .comic_info .btn_wrap .btn_reward {
        background-position: -44px -205px;
    }

    .comic_info .btn_wrap .btn_share {

        background-position: -44px -230px;
    }

    .comic_info .coverBox .btn_wrap a {
        padding-left: 25px;
        margin: 0 20px 0 0;
    }

    .comic_info .jb i {
        display: inline-block;
        width: 18px;
        height: 16px;
        overflow: hidden;
        background: no-repeat 0 -496px;
        margin-right: 5px;
        position: relative;
        top: 4px;
    }
</style>
<%--作品简介div--%>
<div class="wrap cf">
    <div class="comic_info">
        <div class="left">
            <i id="comic_state" class="sign blue">[完结]</i>
            <h1 id="comic_name" class="fl">
                ${requestScope.comic.name}
            </h1>
            <div class="clear">
            </div>
            <div class="coverBox">
                <!--作品封面-->
                <div class="cover" id="cover">
                    <a href="http://www.u17.com/chapter/52776.html" target="_blank">
                        <img id="comic_cover" alt="" src="">
                    </a>
                </div>
                <!--作品封面-->

                <!--作品详情-->
                <!--按钮-->
                <div class="btn_wrap" id="btn_wrap">
                    <div id="open_share">
                        <a class="btn_share" href="javascript:;" style="display: block">分享</a>
                        <%--分享的下拉框--%>
                        <div class="share-down" id="share-down">
                            <div style="height: 40px;">
                                <div class="share-btn b-icon b-icon-share weibo" id="btn_weibo" title="分享到微博"></div>
                                <div class="share-btn b-icon b-icon-share tieba" id="btn_baidu" title="分享到贴吧"></div>
                                <div class="share-btn b-icon b-icon-share qzone" id="btn_qqzone" title="分享到QQ空间"></div>
                            </div>
                            <div class="share-head">将漫画连接贴到博客或论坛</div>
                            <ul style="margin: auto">
                                <li class="share-address">
                                    <label style="float: left">漫画地址</label>
                                    <input type="text" name="video_address" style="float: left" readonly="" value=""/>
                                    <span class="address-copy">复制</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!--按钮 end-->
            </div>
            <div class="info">
                <!--top-->
                <div class="top">
                    <div class="line1">
                        类别：<span id="comic_tag">${requestScope.tags}</span>
                        作者：<span id="comic_author">${requestScope.comic.author}</span>
                        总点击：<i>${requestScope.comic.click}</i>
                    </div>
                    <div class="line2">
                    </div>
                </div>
                <!--top end -->
                <div class="clear">
                </div>
                <!--作品描述-->
                <p class="words" id="words">
                    ${requestScope.comic.description}
                </p>

                <!--作品描述 end-->
                <!--按钮-->
                <div class="btn_wrap" id="btn_big">
                    <a class="btn_start" id="start_read" href=""><em
                            id="start_read_em"></em><strong>开始阅读</strong></a>
                    <%--<a class="btn_continue" id="continue_read" style="display: none;"--%>
                    <%--href="javascript:;"><strong>继续阅读</strong></a>--%>
                    <span id="bookrack">
                        <a id="addCollection" class="btn_stored"
                           href="javascript:;">
                            <strong>加入收藏</strong>
                            <span>+<i>406018</i></span>
                        </a>
                    </span>

                </div>
                <!--按钮 end-->
            </div>
            <!--作品详情 end-->
        </div>
    </div>
</div>


<%--分享的style--%>
<style>

    /*复制地址*/
    .share-address {
        line-height: 20px;
        margin-bottom: 16px;
        zoom: 1;
    }

    /*标题*/
    .share-head {
        font-size: 18px;
        color: #333;
        margin: 20px 0;
        line-height: 24px;
    }

    .share-down {
        display: none;
        margin-top: 30px;
        position: absolute;
        background-color: #FFE4C0;
        text-align: left;
        width: 385px;
        float: left;
        border: solid thin #9A2828;
    }

    /*分享的按钮*/
    .share-btn {
        width: 40px;
        height: 40px;
        float: left;
        margin-right: 35px;
    }

    .b-icon.b-icon-share.tieba {
        background-position: -1100px -524px;
    }

    .b-icon.b-icon-share.weibo {
        background-position: -1100px -459px;
    }

    .b-icon.b-icon-share.qzone {
        background-position: -1099px -587px;
    }

    .b-icon.b-icon-share {
        width: 40px;
        height: 40px;
        background: transparent url(http://static.hdslb.com/images/base/icons.png) no-repeat top left;
    }

    .b-icon {
        display: inline-block;
        vertical-align: middle;
        width: 12px;
        height: 12px;
        background: url(http://static.hdslb.com/images/base/icons.png) no-repeat;
    }

    .b-icon {
        display: inline-block;
        vertical-align: middle;
        width: 12px;
        height: 12px;
        background: url(http://static.hdslb.com/images/base/icons.png) no-repeat;
    }
</style>

<%--章节列表style--%>
<style>

    /*章节框*/
    .chapterlist_box {
        border: 5px solid #e3e3e3;
        position: relative;
        padding: 2px 10px;
        width: 1000px;
        left: 20%
    }

    /*框的四个角*/
    .chapterlist_box b.tl {
        left: -5px;
        top: -5px;
    }

    .chapterlist_box b.tr {
        border-width: 5px 5px 0 0;
        right: -5px;
        top: -5px;
    }

    .chapterlist_box b.bl {
        border-width: 0 0 5px 5px;
        bottom: -5px;
        left: -5px;
    }

    .chapterlist_box b.br {
        border-width: 0 5px 5px 0;
        right: -5px;
        bottom: -5px;
    }

    .chapterlist_box b {
        position: absolute;
        width: 11px;
        height: 11px;
        overflow: hidden;
        border: 5px solid #000;
        border-right-width: 0;
        border-bottom-width: 0;
    }

    .chapterlist_box li {
        height: 47px;
        background: url(http://k.static.u17i.com/v4/www/images/comicinfo/chapter_line.png) repeat-x left bottom;
        padding-left: 15px;
        color: #999;
        font: 12px/46px "Tahoma";
        float: left;
        width: 210px;
        position: relative;
    }

    .chapterlist_box li a {
        padding-left: 15px;
        background: url(http://k.static.u17i.com/v4/www/images/comicinfo/cs_pic.png) no-repeat -55px -352px;
        float: left;
        color: #09d;
        font: 14px/46px "宋体";
        margin-right: 5px;
        position: relative;
        height: inherit;
        overflow: hidden;
    }

    .cf:after {
        content: "";
        display: block;
        height: 0;
        clear: both;
        visibility: hidden;
    }

    /*章节排序*/
    .seqdown {
        background: #d7ebfb url(http://k.static.u17i.com/v4/www/images/comicinfo/cs_pic.png) no-repeat 65px -376px;
    }

    .sequp {
        background: #d7ebfb url(http://k.static.u17i.com/v4/www/images/comicinfo/cs_pic.png) no-repeat 65px -358px;
    }

    .seq {
        display: inline-block;
        height: 22px;
        margin-right: 10px;
        border-radius: 2px;
        -webkit-transition: none;
        -moz-transition: none;
        -o-transition: none;
        transition: none;
        padding: 0 18px 0 12px;
    }

</style>
<%--章节列表--%>
<div class="chapterlist">

    <div class="chapterlist_box">
        <a class="seq sequp" title="当前顺序：正序" id="comicOrder" href="javascript:;">章节顺序</a>
        <b class="tl"></b>
        <b class="tr"></b>
        <b class="bl"></b>
        <b class="br"></b>
        <ul id="chapter_box" class="cf">

        </ul>
    </div>
</div>
<%--分页style--%>
<style>
    .page .current {
        background: #7cca1c;
        color: #fff;
        border: 1px solid #63a612;
    }

    .page a {
        font-family: 'verdana', serif;
        color: #191919;
        display: inline-block;
        height: 30px;
        padding: 0 10px;
        border: 1px solid #e0e0e0;
        background: #f6f6f6;
        margin-right: 4px;
    }
</style>
<%--评论区--%>
<style>

    .comic_comment ul > li:first-child {
        border-top: 1px solid #e5e9ef;
    }

    .comic_comment ul li.main-floor {
        border-top: 1px solid #e5e9ef;
        margin-left: 85px;
        position: relative;
    }

    .comic_comment ul li.main-floor span.uname {
        color: #00a0d8;
    }

    .comic_comment ul li .facebox {
        position: absolute;
        left: -85px;
        width: 60px;
    }

    .comic_comment ul li .facebox img {
        width: 48px;
        height: 48px;
        border-radius: 48px;
        margin: 0 auto;
        display: block;
        border: 1px solid #e5e9ef;
    }

    .comic_comment ul li .content {
        line-height: 20px;
        padding: 2px 0;
        font-size: 14px;
        text-shadow: none;
        overflow: hidden;
        word-wrap: break-word;
    }

    /*回复*/
    .comic_comment .reply ul li img {
        padding-right: 10px;
        float: left;
        width: 24px;
        height: 24px;
        border-radius: 40px;
    }

    /*回复框*/
    .comic_comment .dcmp_content .user_face {
        float: left;
        width: 48px;
        height: 48px;
    }

    .comic_comment .dcmp_content .user_face img {
        width: 100%;
        height: 100%;
        border-radius: 48px;
    }

    .comic_comment #dcmp_textarea_container {
        position: relative;
        margin-left: 88px;
        margin-right: 102px;
    }

    .comic_comment .ipt-txt {
        font-size: 12px;
        background-color: #f4f5f7;
        border: 1px solid #e5e9ef;
        overflow: auto;
        border-radius: 4px;
        color: #555;
        width: 500px;
        height: 52px;
        transition: 0s;
    }

    .comic_comment #dcmp_textarea_container button {
        width: 120px;
        height: 50px;
        font-size: 14px;
        color: #fff;
    }

    /*表情框*/
    .comic_comment .biaoqing_box {
        display: none;
        overflow: hidden;
        background: #fff;
        border: 1px solid #e5e9ef;
        margin-bottom: 10px;
        padding: 10px;
        border-radius: 5px;
        width: 450px;
        float: left;
        position: absolute;
        top: 25px;
        z-index: 100;
    }
</style>
<div class="comic_comment" style="margin-left: 15%">
    <div id="pagination2" class="page"></div>
    <ul id="comment_list" style="margin-top: 50px">
        <li class="main-floor">
            <div class="facebox">
                <img src="http://i2.hdslb.com/bfs/face/7ddf84aa8c0290fa415d684cae77bbbdd99033cf.jpg_52x52.jpg"
                     class="face">
            </div>
            <div class="t">
                <span>XYSG</span>
            </div>
            <div class="content">up必火(=・ω・=)</div>
            <div class="elinfo">
                <span class="floor-num">#10</span>
                <span class="floor-date">2016-04-16 15:22</span>
                <i class="report">
                    <a href="javascript:;" class="huifu">回复</a>
                </i>
            </div>
            <div class="reply">
                <ul class="re_ul">
                    <li>
                        <img src="http://i1.hdslb.com/bfs/face/8a4b4e98980533fc216345aa95526368e854f293.jpg_52x52.jpg">
                        <div class="re_cnt">
                            <div class="w">
                                <span>Erdman</span>
                                <span class="content">回复<a href="javascript:;">@破碎面具</a>:同求(°∀°)ﾉ</span>
                            </div>
                            <div class="info">
                                <p>
                                    <span class="floor-date">2015-12-25 07:05</span>
                                    <a class="re_ta" href="javascript:;;">回复</a>
                                </p>
                            </div>
                        </div>
                    </li>
                    <li>
                        <img src="http://i1.hdslb.com/bfs/face/8a4b4e98980533fc216345aa95526368e854f293.jpg_52x52.jpg">
                        <div class="re_cnt">
                            <div class="w">
                                <span>Erdman</span>
                                <span class="content">回复<a href="javascript:;">@破碎面具</a>:同求(°∀°)ﾉ</span>
                            </div>
                            <div class="info">
                                <p>
                                    <span class="floor-date">2015-12-25 07:05</span>
                                    <a class="re_ta" href="javascript:;;">回复</a>
                                </p>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </li>
    </ul>
    <div id="pagination" class="page"></div>
    <%--回复框--%>
    <div class="dcmp_content" style="margin-top: 20px;display: none">
        <div class="user_face">
            <img class="face_avatar" src="http://i1.hdslb.com/bfs/face/8035a6b6aa80beb39f321b0da7484a29b5bd8d14.jpg">
        </div>
        <div id="dcmp_textarea_container" class="dcmp-textarea-container">
            <textarea cols="80" name="msg" rows="5" id="comment_text"
                      placeholder="请自觉遵守互联网相关的政策法规，严禁发布色情、暴力、反动的言论。"
                      class="ipt-txt"></textarea>
            <button type="submit" class="b-btn submit-comment">发表评论</button>
            <div class="dcmp_post">
                <div class="ywz-wrapper" style="position: relative;">
                    <div class="biaoqing_box">
                        <a>(⌒▽⌒)</a><a>（￣▽￣）</a><a>(=・ω・=)</a><a>(｀・ω・´)</a><a>(〜￣△￣)〜</a><a>(･∀･)</a><a>(°∀°)ﾉ</a><a>(￣3￣)</a><a>╮(￣▽￣)╭</a><a>(
                        ´_ゝ｀)</a><a>←_←</a><a>→_→</a><a>(&lt;_&lt;)</a><a>(&gt;_&gt;)</a><a>(;¬_¬)</a><a>("▔□▔)/</a><a>(ﾟДﾟ≡ﾟдﾟ)!?</a><a>Σ(ﾟдﾟ;)</a><a>Σ(
                        ￣□￣||)</a><a>(´；ω；`)</a><a>（/TДT)/</a><a>(^・ω・^
                        )</a><a>(｡･ω･｡)</a><a>(●￣(ｴ)￣●)</a><a>ε=ε=(ノ≧∇≦)ノ</a><a>(´･_･`)</a><a>(-_-#)</a><a>（￣へ￣）</a><a>(￣ε(#￣)
                        Σ</a><a>ヽ(`Д´)ﾉ</a><a>(╯°口°)╯(┴—┴</a><a>（#-_-)┯━┯</a><a>_(:3」∠)_</a><a>(笑)</a><a>(汗)</a><a>(泣)</a><a>(苦笑)</a>
                    </div>
                    <a class="ywz">(・ω・) 颜文字</a></div>
                <div class="dcmp_userinfo"></div>
            </div>
        </div>
    </div>
</div>
<%--评论相关js--%>
<%--监控元素出现--%>
<script src="js/jquery.onAppear.js"></script>
<%--分页插件--%>
<script src="js/jquery-page5.js"></script>
<script src="js/comment.js"></script>

<script type="text/javascript">
    //
    //漫画相关信息
    var comic_name = '${requestScope.comic.name}';
    var g_comic_id = ${requestScope.comic.id};

    //分享模块
    var ShareMoudle = {
        /**
         * 默认参数
         */
        defaultconfig: {
            title: document.title + "分享的漫画《" + comic_name + "》",//标题
            url: location.href,//分享的网址
            desc: "一起来看漫画吧！",
            pic: "",
            summary: "",
            shortTitle: "",
            searchPic: !1,
            appkey: {
                weibo: "2841902482",
                qqweibo: "84435a83a11c484881aba8548c6e7340"
            },
            weiboTag: ""
        },
        /**
         * 初始化分享模块
         */
        showlist: function () {
            var theModule = this;
            //分享按钮
            var shareDiv = $('#open_share');
            //分享下拉框
            var share_down = $('#share-down');
            //悬浮弹出下拉框
            var enterfunc = function () {
                share_down.stop().slideDown(200);
            };
            var leavefunc = function () {
                share_down.stop().slideUp(200);
            };
            this.hoverDelay(shareDiv, enterfunc, leavefunc);

            //分享按钮
            var btn_weibo = share_down.find('#btn_weibo');
            var btn_baidu = share_down.find('#btn_baidu');
            var btn_qqzone = share_down.find('#btn_qqzone');
            //微博按钮被点击
            btn_weibo.click(function () {
                var conf = theModule.defaultconfig;
                theModule.openShareWindow("http://service.weibo.com/share/share.php?", {
                    url: conf.url,
                    type: "3",
                    count: "1",
                    appkey: conf.appkey.weibo,
                    title: conf.weiboTag + conf.title,
                    pic: conf.pic,
                    searchPic: conf.searchPic,
                    ralateUid: "",
                    language: "zh_cn",
                    rnd: (new Date).valueOf()
                });
            });
            btn_baidu.click(function () {
                var conf = theModule.defaultconfig;
                theModule.openShareWindow("http://tieba.baidu.com/f/commit/share/openShareApi?", {
                    title: conf.title,
                    url: conf.url,
                    uid: 726865,
                    to: "tieba",
                    type: "text",
                    relateUid: "",
                    pic: conf.pic,
                    key: "",
                    sign: "on",
                    desc: "",
                    comment: conf.desc
                });
            });
            btn_qqzone.click(function () {
                var conf = theModule.defaultconfig;
                theModule.openShareWindow("http://sns.qzone.qq.com/cgi-bin/qzshare/cgi_qzshare_onekey?", {
                    url: conf.url,
                    showcount: 1,
                    desc: conf.desc,
                    summary: conf.summary,
                    title: conf.title,
                    site: "www.bi.com",
                    pics: conf.pic,
                    style: "203",
                    width: 98,
                    height: 22
                });
            });
        },
        /**
         * 悬浮响应
         * @param object 触发的元素
         * @param func_enter 进入元素区域的方法
         * @param func_leave 离开元素区域的方法
         * @param delay 延时
         */
        hoverDelay: function (object, func_enter, func_leave, delay) {
            var clock = null;
            object.hover(function () {
                clearTimeout(clock);
                clock = setTimeout(function () {
                    func_enter && func_enter()
                }, delay || 300)
            }, function () {
                clearTimeout(clock);
                clock = setTimeout(function () {
                    func_leave && func_leave()
                }, delay || 300)
            })
        },
        /**
         * 弹出分享窗口
         * @param baseurl
         * @param args
         * @returns {boolean}
         */
        openShareWindow: function (baseurl, args) {
            var url = [], g;
            for (g in args) {
                url.push(g + "=" + encodeURIComponent(args[g] || ""))
            }
            url = baseurl + url.join("&");
            window.open(url, "", 'width=700, height=680, top=0, left=0, toolbar=no, menubar=no, scrollbars=no, location=yes, resizable=no, status=no');
            return !1
        }
    };

    //收藏模块
    var collectMoudle = {
        showlist: function () {
            var haveUserCollect =${requestScope.haveUserCollect};
            var collectCount =${requestScope.collectCount};

            var addCollection = $('#addCollection');

            //收藏按钮初始化
            var btn_text = addCollection.find("strong");
            //收藏数量
            var i_count = addCollection.find('i');
            i_count.text(collectCount);

            function checkCollection() {
                if (haveUserCollect) {
                    //用户已经收藏
                    btn_text.text("取消收藏");
                } else {
                    //用户没有收藏
                    btn_text.text("加入收藏");
                }
            }

            if (isLogin()) {
                checkCollection();
            }

            //收藏按钮被点击
            addCollection.click(function () {
                if (isLogin()) {
                    var data = {
                        user_name: g_user_name,
                        comic_id: g_comic_id,
                        type: ""
                    };

                    if (haveUserCollect) {
                        data.type = "delete";//，已经收藏了，就删除收藏
                    } else {
                        data.type = "add";//增加收藏
                    }

                    collectMoudle.myAjax(data, function (data) {
                        //回调函数
                        if (data.result) {
                            //成功
                            i_count.text(data.count);//显示最新的收藏数量
                            haveUserCollect = !haveUserCollect;
                        }
                        checkCollection();
                        openDialog(data.msg, addCollection);
                        console.log(data);
                    });
                } else {
                    //没有登录的情况下
                    openDialog("<a href='" + g_contextPath + "/login.jsp'>登录</a>后再收藏吧", addCollection);
                }
            });


        },
        /**
         * ajax提交收藏和取消收藏要求
         * @param data
         * @param callback
         */
        myAjax: function (data, callback) {
            $.ajax({
                url: "/collect",
                type: "GET",
                dataType: "json",
                timeout: 3000,
                data: data,
                success: function (data, textStatus) {
                    callback(data);
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert(XMLHttpRequest + textStatus + errorThrown);
                }
            });
        }
    };

    //dom标签反转顺序
    function get_chapterlist_order() {
        var obj = document.getElementById("chapter_box");
        var lis = obj.getElementsByTagName("li");
        for (var i = lis.length - 1; i > -1; i--) {
            obj.appendChild(lis[i]);
        }
    }

    $(document).ready(function () {

        //章节盒子
        var chapter_box = $('#chapter_box');

        //封面
        var comic_cover = $('#comic_cover');
        comic_cover.attr("src", '${requestScope.comic.cover}');
        comic_cover.attr("alt", comic_name);

        //阅读按钮
        var startBtn = $('#start_read');
        var start_chapterId =${requestScope.start_chapterId};//该漫画的第一章id
        var history_chapter_id =${requestScope.history_chapterId};//服务器中历史记录阅读到哪一章
        var readId = start_chapterId;//真正要读的章节-【默认是漫画的第一章id】
        if (history_chapter_id > 0) {
            startBtn.text("继续阅读第${requestScope.history_chapter.comic_index}话");
        } else {
            //尝试从cookies中获得chapterId
            var history = historyMoudle.getHistoryComic(g_comic_id);
            if (history.chapter_id != 0) {
                readId = history.chapter_id;
                startBtn.text("继续阅读");
            }
        }
        startBtn.click(function () {//打开新章节
            window.open(g_contextPath + "/read?chapterid=" + readId);
        });


        //连载
        var state = '${requestScope.comic.state}';
        var comic_state = $('#comic_state');
        if (state == '1') {
            comic_state.text('[完结]');
        } else {
            comic_state.text('[连载]');
        }

        //章节
        var chaptersJson = '${requestScope.chapters}';
        var chapters = $.parseJSON(chaptersJson);
        $.each(chapters, function (n, value) {
            var li = $('<li></li>');
            var a = $('<a></a>');
            a.text('第' + value.comic_index + '话,' + value.title);
            a.attr("title", value.updatetime);
            a.attr("href", "/read?chapterid=" + value.id);
            li.append(a);
            chapter_box.append(li);
        });

        //章节排序
        var comicOrder = $('#comicOrder');
        comicOrder.click(function () {
            //设定章节排序
            var cookie = $.cookie('comic_order');
            if (comicOrder.hasClass('seqdown')) {
                //在cookies中记录排序
                $.cookie('comic_order', 'asc', {expires: 60 * 60 * 24 * 30});
                get_chapterlist_order();
                comicOrder.removeClass("seqdown").addClass("sequp").attr('title', '当前顺序：正序');
            } else {
                $.cookie('comic_order', 'desc', {expires: 60 * 60 * 24 * 30});
                get_chapterlist_order();
                comicOrder.addClass("seqdown").removeClass("sequp").attr('title', '当前顺序：倒序');
            }
        });

        //获取默认漫画排序
        //刚进入页面可以沿用之前的排序规则
        var cookie = $.cookie('comic_order');
        if (cookie == 'desc') {
            comicOrder.addClass("seqdown").removeClass("sequp").attr('title', '当前顺序：倒序');
            //数据库输出为正序
            get_chapterlist_order();
        } else {
            comicOrder.removeClass("seqdown").addClass("sequp").attr('title', '当前顺序：正序');
            $.cookie('comic_order', 'asc', {expires: 60 * 60 * 24 * 30});
        }

        //各模块初始化
        ShareMoudle.showlist();
        collectMoudle.showlist();
        historyMoudle.addHistory(g_comic_id, 0);
    });
</script>

</body>
</html>
