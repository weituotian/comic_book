<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="include/top_container.jsp" %>

<%--所谓的主页--%>

<link href="css/index.css" rel="stylesheet" type="text/css">

<%--作品推荐--%>
<div class="scroll_wrap a_level">
    <h2 class="title"><p id="azp">收藏5000以上，2个月稳定更新，不分类型&nbsp;<a href="http://help.u17.com/index.php?doc-view-130"
                                                                target="_blank">[规则]</a></p><a
            href="http://www.u17.com/more/salist_ta.html" target="_blank" class="more" title="更多"></a></h2>
    <div class="content">
        <a href="javascript:;" class="prev" style="display: none;"><span></span><i></i></a>
        <a href="javascript:;" class="next" style="display: none;"><span></span><i></i></a>
        <div class="scroll_comic">
            <ul id="ul_recommand" style="left: 0;">
                <li>
                    <em class="ico_dasai"></em>
                    <a href="http://www.u17.com/comic/109041.html" target="_blank" class="bg_comic">
                        <img src="http://cover.u17i.com/2016/01/4319696_1452855193_6aFgvIIzLpF6.small.jpg">
                    </a>
                    <a href="http://www.u17.com/comic/109041.html" target="_blank" class="comic_name" title="CDloading">
                        CDloading </a>
                    <span>作者自传</span>
                </li>
                <li>
                    <em class="ico_update"></em>
                    <a href="http://www.u17.com/comic/74679.html" target="_blank" class="bg_comic"><img
                            src="http://cover.u17i.com/2014/12/4421956_1419903032_L2DUU5Cb5uRl.small.jpg"></a>
                    <a href="http://www.u17.com/comic/74679.html" target="_blank" class="comic_name" title="太平广记">
                        <i class="ico_rec"></i>太平广记 </a>
                    <span>民间灵异故事集</span>
                </li>
                <li>
                    <em class="ico_tiaoman"></em>
                    <a href="http://www.u17.com/comic/120003.html" target="_blank" class="bg_comic"><img
                            src="http://cover.u17i.com/2016/04/14197110_1460006250_3Xb1pT27v9n3.small.jpg"></a>
                    <a href="http://www.u17.com/comic/120003.html" target="_blank" class="comic_name" title="非人哉">
                        <i class="ico_rec"></i>非人哉 </a>
                    <span>与时俱进的妖精们</span>
                </li>
                <li>
                    <em class="ico_dasai"></em>
                    <a href="http://www.u17.com/comic/103295.html" target="_blank" class="bg_comic"><img
                            src="http://cover.u17i.com/2015/08/11520118_1439197686_AsXL1F8YQLs1.small.jpg"></a>
                    <a href="http://www.u17.com/comic/103295.html" target="_blank" class="comic_name" title="笨柴兄弟">
                        <i class="ico_rec"></i>笨柴兄弟 </a>
                    <span>两只萌萌的柴犬</span>
                </li>
                <li>
                    <em class="ico_update"></em>
                    <a href="http://www.u17.com/comic/113428.html" target="_blank" class="bg_comic"><img
                            src="http://cover.u17i.com/2016/03/11394837_1458265266_LMymS5mg1F01.small.jpg"></a>
                    <a href="http://www.u17.com/comic/113428.html" target="_blank" class="comic_name" title="互撸大漫画">
                        互撸大漫画 </a>
                    <span>挑战你的内涵尺寸</span>
                </li>
                <li>
                    <em class="ico_update"></em>
                    <a href="http://www.u17.com/comic/110775.html" target="_blank" class="bg_comic"><img
                            src="http://cover.u17i.com/2015/11/9505260_1446961778_YYG9ph7gV9qh.small.jpg"></a>
                    <a href="http://www.u17.com/comic/110775.html" target="_blank" class="comic_name" title="灵异档案">
                        灵异档案 </a>
                    <span>各种恐怖故事合集</span>
                </li>
                <li>
                    <em class="ico_dasai"></em>
                    <a href="http://www.u17.com/comic/99059.html" target="_blank" class="bg_comic"><img
                            src="http://cover.u17i.com/2015/06/4093128_1433996640_tn848Y72OebK.small.jpg"></a>
                    <a href="http://www.u17.com/comic/99059.html" target="_blank" class="comic_name" title="星球拟鸟">
                        <i class="ico_rec"></i>星球拟鸟 </a>
                    <span>记得佩戴望远镜观看</span>
                </li>
                <li>
                    <em class="ico_update"></em>
                    <a href="http://www.u17.com/comic/87320.html" target="_blank" class="bg_comic"><img
                            src="http://cover.u17i.com/2016/03/1680_1456970602_Q5UKY8Ciu8Gq.small.jpg"></a>
                    <a href="http://www.u17.com/comic/87320.html" target="_blank" class="comic_name" title="天下第几">
                        <i class="ico_rec"></i>天下第几 </a>
                    <span>没有更强只有最强</span>
                </li>
                <li>
                    <em class="ico_update"></em>
                    <a href="http://www.u17.com/comic/110530.html" target="_blank" class="bg_comic"><img
                            src="http://cover.u17i.com/2016/03/12692117_1458390947_LCbMwbbjwnBm.small.jpg"></a>
                    <a href="http://www.u17.com/comic/110530.html" target="_blank" class="comic_name" title="论怪物军团作死方法">
                        <i class="ico_rec"></i>论怪物军团作死 </a>
                    <span>唤醒怪物的人</span>
                </li>
                <li>
                    <em class="ico_update"></em>
                    <a href="http://www.u17.com/comic/12966.html" target="_blank" class="bg_comic"><img
                            src="http://cover.u17i.com/2011/03/512339_1299759846_kmZzKYcq3Rfc.small.jpg"></a>
                    <a href="http://www.u17.com/comic/12966.html" target="_blank" class="comic_name" title="上班了没？">
                        上班了没？ </a>
                    <span>各种搞笑生活小故事</span>
                </li>
                <li>
                    <em class="ico_update"></em>
                    <a href="http://www.u17.com/comic/81110.html" target="_blank" class="bg_comic"><img
                            src="http://cover.u17i.com/2015/08/4774213_1439220350_bdd9686XT8Mm.small.jpg"></a>
                    <a href="http://www.u17.com/comic/81110.html" target="_blank" class="comic_name" title="逗比生活">
                        逗比生活 </a>
                    <span>黄医湿和两个小护士</span>
                </li>
                <li>
                    <em class="ico_update"></em>
                    <a href="http://www.u17.com/comic/110523.html" target="_blank" class="bg_comic"><img
                            src="http://cover.u17i.com/2016/02/10696274_1455275620_Z7t7m0t6Z70l.small.jpg"></a>
                    <a href="http://www.u17.com/comic/110523.html" target="_blank" class="comic_name" title="妖谶">
                        妖谶 </a>
                    <span>结束这场千年的孽缘</span>
                </li>
                <li>
                    <em class="ico_update"></em>
                    <a href="http://www.u17.com/comic/114396.html" target="_blank" class="bg_comic"><img
                            src="http://cover.u17i.com/2016/05/130912_1462619870_o8zrAC83RU3i.small.jpg"></a>
                    <a href="http://www.u17.com/comic/114396.html" target="_blank" class="comic_name" title="恶魔同檐">
                        恶魔同檐 </a>
                    <span>我的恶魔雇主</span>
                </li>
                <li>
                    <em class="ico_dasai"></em>
                    <a href="http://www.u17.com/comic/106403.html" target="_blank" class="bg_comic"><img
                            src="http://cover.u17i.com/2016/04/862476_1459524647_0Oy3obWf2F0b.small.jpg"></a>
                    <a href="http://www.u17.com/comic/106403.html" target="_blank" class="comic_name" title="腐眼看世界">
                        腐眼看世界 </a>
                    <span>腐属性双胞胎兄妹</span>
                </li>
                <li>
                    <em class="ico_update"></em>
                    <a href="http://www.u17.com/comic/47434.html" target="_blank" class="bg_comic"><img
                            src="http://cover.u17i.com/2014/02/1432641_1391584686_Gb63w6XCwpwg.small.jpg"></a>
                    <a href="http://www.u17.com/comic/47434.html" target="_blank" class="comic_name" title="女神的俸禄">
                        女神的俸禄 </a>
                    <span>关于女仆的少年漫画</span>
                </li>
                <li>
                    <em class="ico_update"></em>
                    <a href="http://www.u17.com/comic/119639.html" target="_blank" class="bg_comic"><img
                            src="http://cover.u17i.com/2016/05/2667640_1463334917_xuXFP0bAtf9U.small.jpg"></a>
                    <a href="http://www.u17.com/comic/119639.html" target="_blank" class="comic_name" title="蛇君">
                        蛇君 </a>
                    <span>嫁入蛇妖的男人</span>
                </li>
                <li>
                    <em class="ico_update"></em>
                    <a href="http://www.u17.com/comic/86971.html" target="_blank" class="bg_comic"><img
                            src="http://cover.u17i.com/2016/01/4632044_1454160646_uUpauuZ0Bphl.small.jpg"></a>
                    <a href="http://www.u17.com/comic/86971.html" target="_blank" class="comic_name" title="看不见的爱情">
                        看不见的爱情 </a>
                    <span>心痛纯爱物语</span>
                </li>
                <li>
                    <em class="ico_update"></em>
                    <a href="http://www.u17.com/comic/43411.html" target="_blank" class="bg_comic"><img
                            src="http://cover.u17i.com/2016/04/768014_1459653062_26vyWn6LcSgI.small.jpg"></a>
                    <a href="http://www.u17.com/comic/43411.html" target="_blank" class="comic_name" title="屠宰场">
                        屠宰场 </a>
                    <span>深夜尸体面具屠宰事</span>
                </li>
                <li>
                    <em class="ico_update"></em>
                    <a href="http://www.u17.com/comic/80098.html" target="_blank" class="bg_comic"><img
                            src="http://cover.u17i.com/2015/11/4678945_1448596864_b2zIDIb2KB5b.small.jpg"></a>
                    <a href="http://www.u17.com/comic/80098.html" target="_blank" class="comic_name" title="界之间">
                        界之间 </a>
                    <span>借尸还魂的少年</span>
                </li>
                <li>
                    <em class="ico_dasai"></em>
                    <a href="http://www.u17.com/comic/102952.html" target="_blank" class="bg_comic"><img
                            src="http://cover.u17i.com/2016/05/10671998_1462801215_f0D17a9i079r.small.jpg"></a>
                    <a href="http://www.u17.com/comic/102952.html" target="_blank" class="comic_name" title="吸血萌物">
                        吸血萌物 </a>
                    <span>萌就是正义</span>
                </li>
                <li>
                    <em class="ico_dasai"></em>
                    <a href="http://www.u17.com/comic/103136.html" target="_blank" class="bg_comic"><img
                            src="http://cover.u17i.com/2015/08/9641392_1439199007_1uC2zJIgCbcY.small.jpg"></a>
                    <a href="http://www.u17.com/comic/103136.html" target="_blank" class="comic_name" title="爆笑禅师体漫画">
                        爆笑禅师体漫画 </a>
                    <span>尘世中迷途漫画人</span>
                </li>

            </ul>
        </div>
    </div>
</div>

<%--排行榜--%>
<div class="rank_box shaonian_rank">
    <a href="http://comic.u17.com/rank/a1_r2_t1_p1.html" target="_blank" class="top_link" title="少年漫画周榜[点击查看更多]"></a>
    <ul id="rank1" style="list-style-type: none">
        <li class="q first"><i></i>
            <a href="http://www.u17.com/comic/3166.html" target="_blank" class="name" title="类别:少年/魔幻">镇魂街</a>
            <em>508668</em>
        </li>
        <li class="q second"><i></i>
            <a href="http://www.u17.com/comic/120824.html" target="_blank" class="name" title="类别:少年/魔幻/动作">西行纪（全彩周更</a><em>74481</em>
        </li>
        <li class="q third"><i></i>
            <a href="http://www.u17.com/comic/99874.html" target="_blank" class="name"
               title="类别:少年/魔幻/动作">妖神记（全彩）</a><em>62995</em>
        </li>
        <li>
            <i>4</i>
            <a href="http://www.u17.com/comic/4885.html" target="_blank" class="name" title="类别:少年/魔幻/动作/战争">神明之胄</a>
            <em>57917</em>
        </li>
        <li><i>5</i>
            <a href="http://www.u17.com/comic/11072.html" target="_blank" class="name" title="类别:少年/魔幻/动作">黑瞳</a><em>52865</em>
        </li>
        <li><i>6</i>
            <a href="http://www.u17.com/comic/101176.html" target="_blank" class="name" title="类别:少年/魔幻/动作">帝世纪 （全彩
                周</a><em>48552</em>
        </li>
        <li><i>7</i>
            <a href="http://www.u17.com/comic/75859.html" target="_blank" class="name" title="类别:少年/魔幻/动作">噬规者</a><em>45336</em>
        </li>
        <li><i>8</i>
            <a href="http://www.u17.com/comic/5553.html" target="_blank" class="name" title="类别:少年/搞笑">十万个冷笑话</a><em>41202</em>
        </li>
        <li><i>9</i>
            <a href="http://www.u17.com/comic/4240.html" target="_blank" class="name"
               title="类别:少年/生活/恐怖">日渐崩坏的世界</a><em>33332</em>
        </li>
        <li><i>10</i>
            <a href="http://www.u17.com/comic/1383.html" target="_blank" class="name"
               title="类别:少年/搞笑/生活/科幻">星STAR</a><em>31077</em>
        </li>
        <li><i>11</i>
            <a href="http://www.u17.com/comic/195.html" target="_blank" class="name" title="类别:少年/动作/科幻/战争">雏蜂</a><em>29480</em>
        </li>
        <li><i>12</i>
            <a href="http://www.u17.com/comic/8805.html" target="_blank" class="name" title="类别:少年/魔幻/动作">虎x鹤
                妖师录</a><em>28644</em>
        </li>
        <li><i>13</i>
            <a href="http://www.u17.com/comic/71064.html" target="_blank" class="name" title="类别:少年/魔幻/动作">斗罗大陆</a><em>28641</em>
        </li>
        <li><i>14</i>
            <a href="http://www.u17.com/comic/64715.html" target="_blank" class="name" title="类别:少年/动作/科幻">无视者</a><em>27063</em>
        </li>
        <li><i>15</i>
            <a href="http://www.u17.com/comic/94607.html" target="_blank" class="name"
               title="类别:少年/魔幻/恋爱/动作">迷失在世界尽头</a><em>24828</em>
        </li>
    </ul>
</div>

<script src="js/index.js"></script>
</body>
</html>
