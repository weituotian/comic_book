<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="include/top_container.jsp" %>

<%--注册页面--%>

<style type="text/css">
    input.error {
        border: 1px dashed red;
        background: yellow;
        padding: 2px;
    }

    select.error {
        border: 1px dashed red;
        background: yellow;
        padding: 2px;
    }

    .error {
        color: red;
        font-size: 11px;
    }

    /*验证通过样式*/
    .valid {
        color: #00F;
        font-size: 11px;
    }
</style>
<%--注册表单--%>
<div class="row-fluid">
    <div class="span12">
        <form class="form-horizontal center-block" id="reg_form">
            <div class="control-group">
                <label class="control-label" for="reg_name">用户名</label>
                <div class="controls">
                    <input id="reg_name" name="reg_name" type="text" value=""/>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="reg_email">邮箱</label>
                <div class="controls">
                    <input id="reg_email" name="reg_email" type="text" value="ddd"/>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="reg_password">密码</label>
                <div class="controls">
                    <input id="reg_password" name="reg_password" type="password" value="aaa"/>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="reg_password2">确认密码</label>
                <div class="controls">
                    <input id="reg_password2" name="reg_password2" type="password" value="aasca"/>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="reg_vali">验证码：</label>
                <div class="controls">
                    <input id="reg_vali" name="reg_vali" type="text" value=""/>
                </div>
                <div class="controls">
                    <img src="/valicode" width="200px" height="50px" id="valicodeBox"/>
                </div>
            </div>
            <div class="control-group">
                <div class="controls">
                    <button type="button" class="btn" id="reg_button">注册</button>
                </div>
            </div>
        </form>
    </div>
</div>

<style type="text/css">
    /*密码强度框的css*/
    .safe_line {
        margin-right: 10px;
        float: left;
        margin-top: 7px;
        margin-left: 15px;
        width: 30px;
        height: 5px;
        border-radius: 5px;
    }
</style>

<%--密码强度检测的插件--%>
<script type="text/javascript" src="js/jquery.pwdDetect.js"></script>
<script type="text/javascript">
    /**
     * 文档加载完毕运行
     **/
    $(function () {
        //表单的控件初始化
        var myDialog = $('#dialog-message');
        var reg_name = $("#reg_name"),
                reg_email = $("#reg_email"),
                reg_password = $("#reg_password");
        var reg_button = $("#reg_button");
        var reg_form = $('#reg_form');

        //验证码相关
        var reg_vali = $('#reg_vali');
        var valicodeBox = $('#valicodeBox');
        //注册按钮点击
        reg_button.click(function () {
            //表单提交
            reg_form.submit();
        });

        //开启密码强度检测
        reg_password.pwdDetect({line_class: "safe_line"});
        //失去焦点时候自动验证
        reg_name.blur(function () {
            reg_name.valid();
        });
        reg_email.blur(function () {
            reg_email.valid();
        });
        reg_vali.blur(function () {
            reg_vali.valid();
        });

        //验证
        reg_form.validate({
            //键盘弹起触发的函数
            onkeyup: function (element) {
                //键盘弹起不验证的数组
                var index = ["reg_name", "reg_email", "reg_vali"];
                if ($.inArray(element.name, index) == -1) {//不在这个数组中
                    $(element).valid();
                }
//                if (element.name != "" && element.name!="") {
//                }
            },
            highlight: function (element, errorClass, validClass) {
                $(element).addClass("error").removeClass("valid");
                $(element).parent("div").find("span.ui-icon").addClass("ui-icon-circle-close").removeClass("ui-icon-circle-check");
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).removeClass("error").addClass("valid");
                //$(element).parent("div").find("span.ui-icon").addClass("ui-icon-circle-check").removeClass("ui-icon-circle-close");
            },
            //显示错误的方法
            showErrors: function (errorMap, errorList) {
                this.defaultShowErrors();//默认显示错误的方法
            },
            //提交按钮被点击后执行的函数,需要验证成功
            submitHandler: function (form) {
                //alert("submit");
                //暂时禁用提交按钮
                reg_button.attr("disabled", true);
                reg_button.text("提交中……");
                toReg();
            },
            rules: {
                reg_password: {
                    required: true,
                    rangelength: [5, 10]
                },
                reg_password2: {
                    required: true,
                    equalTo: "#reg_password"
                }
            },
            messages: {
                reg_name: {
                    required: "需要用户名",
                    rangelength: $.format("长度为{0}到{1}"),
                    remote: "用户名已经被注册了！"
                },
                reg_vali: {
                    required: "填写验证码",
                    remote: "验证码不正确！"
                },
                reg_email: {
                    required: "需要邮箱",
                    email: "正确的邮箱呢？",
                    remote: "邮箱已经被注册了！"
                },
                reg_password: {
                    required: "填写密码",
                    rangelength: $.format("长度为{0}到{1}")
                },
                reg_password2: {
                    required: "填写密码",
                    equalTo: "密码不一致"
                }
            },
            //设置错误信息存放标签
            errorElement: "em",
            //放置错误标签的位置
            errorPlacement: function (error, element) {
                //var parentDiv = element.parent("div");
                //error指的是错误标签
                //error.appendTo(parentDiv);
                element.after(error);
                error.before($("<span class='ui-icon' style='display: inline-block'></span>"));
                $(element).valid();//触发验证
            },
            success: function (label, dom) {
                //字段验证成功后的方法
                label.parent("div").find("input").removeClass("error").addClass("valid");
                label.parent("div").find("span.ui-icon").addClass("ui-icon-circle-check").removeClass("ui-icon-circle-close");
                label.addClass("valid").text('ok');
            }
        });

        jQuery.validator.addMethod("stringCheck", function (value, element) {
            return this.optional(element) || /^[a-zA-z][a-zA-Z0-9_]{2,9}$/.test(value);
        }, "用户名以字母开头");

        //添加验证规则
        reg_name.rules("add", {
            required: true,
            rangelength: [5, 10],
            stringCheck: true,
            remote: {
                url: 'reg',
                type: 'GET',
                data: {
                    action: "checkName",
                    toCheckStr: function () {
                        return reg_name.val();
                    }
                },
                dataType: "json",
                dataFilter: function (data, type) {
                    var jsonObj = jQuery.parseJSON(data);
                    return jsonObj.canReg;
                }
            }
        });
        reg_email.rules("add", {
            required: true,
            email: true,
            remote: {
                url: 'reg',
                type: 'GET',
                data: {
                    action: "checkMail",
                    toCheckStr: function () {
                        return reg_email.val();
                    }
                },
                dataType: "json",
                dataFilter: function (data, type) {
                    var jsonObj = jQuery.parseJSON(data);
                    return jsonObj.canReg;
                }
            }
        });
        reg_vali.rules("add", {
            required: true,
            remote: {
                url: '/valicode',
                type: 'POST',
                data: {
                    code: function () {
                        return reg_vali.val();
                    }
                },
                dataType: "json",
                dataFilter: function (data, type) {
                    var jsonObj = jQuery.parseJSON(data);
                    return jsonObj.canReg;
                }
            }
        });

        //邮箱自动填充完整
        reg_email.autocomplete({
            delay: 0,
            autoFocus: true,
            source: function (request, response) {
                var hosts = ['qq.com', '163.com', '263.com', 'sina.com.cn', 'gmail.com', 'hotmail.com'],
                        term = request.term,		//获取用户输入的内容
                        name = term,				//邮箱的用户名
                        host = '',					//邮箱的域名
                        ix = term.indexOf('@'),		//@的位置
                        result = [];				//最终呈现的邮箱列表
                result.push(term);
                //当有@的时候，重新分别用户名和域名
                if (ix > -1) {
                    name = term.slice(0, ix);
                    host = term.slice(ix + 1);
                }
                if (name) {
                    //如果用户已经输入@和后面的域名，
                    //那么就找到相关的域名提示，比如bnbbs@1，就提示bnbbs@163.com
                    //如果用户还没有输入@或后面的域名，
                    //那么就把所有的域名都提示出来
                    var findedHosts = (host ? $.grep(hosts, function (value, index) {
                        return value.indexOf(host) > -1
                    }) : hosts);
                    var findedResult = $.map(findedHosts, function (value, index) {
                        return name + '@' + value;
                    });
                    //数组连接
                    result = result.concat(findedResult);
                }
                response(result);
            }
        });

        //验证码箱子被点击就刷新
        valicodeBox.click(function () {
            valicodeBox.attr("src", "/valicode?t=" + Math.random());
        });

        /**
         * 去注册
         */
        function toReg() {
            $.ajax({
                url: "/reg",
                type: "POST",
                datatype: "json",
                timeout: 3000,
                data: {
                    reg_email: reg_email.val(),
                    reg_name: reg_name.val(),
                    reg_password: reg_password.val()
                },
                success: function (rdata, textStatus) {
                    var data = $.parseJSON(rdata);
                    //设置消息
                    myDialog.find('#msg').html(data.msg);
                    //设置图标
                    if (data.error > 0) {//有错误
                        myDialog.find("#icon").attr("class", "ui-icon ui-icon-circle-close");
                    } else {
                        //注册成功
                        myDialog.find("#icon").attr("class", "ui-icon ui-icon-circle-check");
                        //3秒后跳转到首页
                        setTimeout(function () {
                            window.location.href = "index.jsp";
                        }, 3000);
                        reg_button.text("注册成功");
                    }
                    //打开对话框
                    myDialog.dialog("open");
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert(XMLHttpRequest);
                    alert(textStatus);
                    alert(errorThrown);
                }
            })
        }

    });
</script>

</body>
</html>
