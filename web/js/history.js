/**
 * 提供历史操作的js
 */

//历史模块
var historyMoudle = {
    cookiesData: {expires: 60 * 60 * 24 * 30, path: "/"},
    /**
     * 记录历史记录的实体
     */
    historyOBJ: function (comic_id, chapter_id, time) {
        this.comic_id = comic_id;
        this.chapter_id = chapter_id;
        this.time = time;
    },
    /**
     * 增加或者更新新纪录
     * @param arr
     * @param hobj
     */
    insert: function (arr, hobj) {
        //历史记录有多少条
        var length = arr.length;

        for (var i = 0; i < length; i++) {
            var each = arr[i];
            if (each.comic_id == hobj.comic_id) {
                //如果有就直接更新了
                each.time = hobj.time;
                if (each.chapter_id != 0 && hobj.chapter_id == 0) {
                    //这种情况不更新chapter_id，逻辑上不能将0覆盖已经存在的chapter_id
                } else {
                    each.chapter_id = hobj.chapter_id;
                }
                break;
            }
        }

        //数组排序，按时间从大到小
        arr.sort(function (a, b) {
            return -(a.time - b.time);//如果a的时间比较大，那么返回负数，排前面一点
        });

        //限制历史记录数量
        if (length > 30) {
            arr = arr.slice(0, 20);//从下标0开始到下标19结束
            //返回从原数组中指定开始下标到结束下标之间的项组成的新数组
        }

        if (i == length) {
            //遍历完也没有找到,就直接插入
            arr.push(hobj);
        }
    },
    delete: function (cid) {
        var oldJson = $.cookie('history');
        if (oldJson) {//如果有历史记录
            //获取之前保存的历史记录
            var oldHistorys = $.parseJSON(oldJson);

            var length = oldHistorys.length;

            //遍历并且删除该函数
            for (var i = 0, n = 0; i < length; i++) {
                if (oldHistorys[i].comic_id != cid) {
                    oldHistorys[n++] = oldHistorys[i];
                }
            }
            oldHistorys.length -= 1;

            $.cookie('history', $.toJSON(oldHistorys), this.cookiesData);//真正写入cookies
        }
    },
    /**
     * 增加历史记录
     * @param comic_id
     * @param chapter_id
     */
    addHistory: function (comic_id, chapter_id) {
        if (isLogin()) {
            return;//如果用户已经登录了就不做任何操作
        }

        var curtime = (new Date()).getTime();//当前时间戳
        var newHistory = new historyMoudle.historyOBJ(comic_id, chapter_id, curtime);//新建一个历史记录object

        console.log("之前的：" + $.cookie('history'));
        var oldJson = $.cookie('history');
        if (oldJson) {//如果有历史记录

            //获取之前保存的历史记录
            var oldHistorys = $.parseJSON(oldJson);
            historyMoudle.insert(oldHistorys, newHistory);
            $.cookie('history', $.toJSON(oldHistorys), this.cookiesData);//真正写入cookies

        } else {
            //最开始的历史记录
            var newHistorys = [];
            newHistorys.push(newHistory);
            $.cookie('history', $.toJSON(newHistorys), this.cookiesData);//真正写入cookies
        }
        console.log("之后的：" + $.cookie('history'));

    },
    getHistoryJson: function () {
        var json = $.cookie('history');
        if (json) {
            return json;
        }
        return "";
    },
    getHistoryComic: function (comicId) {
        var oldJson = $.cookie('history');
        if (oldJson) {//如果有历史记录
            //获取之前保存的历史记录
            var oldHistorys = $.parseJSON(oldJson);

            var length = oldHistorys.length;

            //遍历并且删除该函数
            for (var i = 0; i < length; i++) {
                if (oldHistorys[i].comic_id == comicId) {
                    return oldHistorys[i];
                }
            }
        }
        return 0;
    },
    getHistoryCount: function () {
        var oldJson = $.cookie('history');
        if (oldJson) {//如果有历史记录
            var oldHistorys = $.parseJSON(oldJson);
            return oldHistorys.length;
        }
        return 0;
    }
};