/**
 *供主页应用的js
 */

$(document).ready(function () {

    var $ul_recommand = $('#ul_recommand');//作品推荐div
    var rank_ul = $('#rank1');//排行div

    /**
     * ajax请求list
     * @param data
     * @param callback
     */
    function myAjax(data, callback) {
        $.ajax({
            url: "/clist",
            type: "GET",
            dataType: "json",
            timeout: 3000,
            data: data,
            success: function (data, textStatus) {
                callback(data);
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert(XMLHttpRequest + textStatus + errorThrown);
            }
        });
    }

    /**
     * 负责图片轮播
     */
    var scrollMoudle = {
        showlist: function () {
            var pageNum = 3;

            var $scroll_wrap = $(".scroll_wrap");//每个最外层div

            //遍历每个$scroll_wrap
            $scroll_wrap.each(function () {
                var $this = $(this);

                var $scrollBox = $this.find(".scroll_comic");//装ul的容器

                var $ul = $scrollBox.children("ul");
                var $next = $scrollBox.siblings(".next");//往右滚按钮
                var $pre = $scrollBox.siblings(".prev");//向左滚按钮

                var page = 1;//从1到3

                var moveWidth = $scrollBox.width();//翻一页所需要移动的距离
                function scrollPage(page) {
                    $ul.animate({left: -(page - 1) * moveWidth}, "slow", function () {
                    })
                }

                $next.click(function () {//往右滚按钮被点击
                    if (page < pageNum) {
                        page++;
                        scrollPage(page);
                    }
                });

                $pre.click(function () {//往左滚按钮被点击
                    if (page > 1) {
                        page--;
                        scrollPage(page);
                    }
                });

                /* onhover时候显示和隐藏按钮 */
                $this.mouseenter(function () {
                    $next.show();
                    $pre.show();

                    $ul.stop(true);
                });
                $this.mouseleave(function () {
                    $next.hide();
                    $pre.hide();

                    autoScroll(page);
                });

                /*自动滚动*/
                var forwardRight = true;//现在是否向右滚动
                /**
                 * 自动滚动
                 * @param curPage 现在页面
                 */
                function autoScroll(curPage) {
                    var targetPage;//下一个目标页面
                    if (curPage == 1) {
                        forwardRight = true;
                        targetPage = 2;
                    } else if (1 < curPage && curPage < pageNum) {
                        if (forwardRight) {
                            targetPage = curPage + 1;
                        } else {
                            targetPage = curPage - 1;
                        }
                    } else if (curPage == pageNum) {
                        forwardRight = false;
                        targetPage = pageNum - 1;
                    }

                    $ul.animate({left: -(targetPage - 1) * moveWidth}, 5000, function () {
                        page = targetPage;
                        autoScroll(targetPage);
                    });
                }

                autoScroll(1);//一开始调用函数
            });

        }
    };

    var parseMoudle = {
        parseComic: function (data) {
            //console.log(data);
            $ul_recommand.empty();//清空元素内容物
            $.each(data, function (n, value) {
                var url = "/comic?comicid=" + value.id;

                var li = $('<li><em class="ico_dasai"></em></li>').appendTo($ul_recommand);
                var a1 = $("<a target='_blank' class='bg_comic'></a>").attr("href", url).appendTo(li);
                var img = $('<img/>').attr("src", value.cover).appendTo(a1);
                var a_name = $('<a target="_blank" class="comic_name"></a>').attr("href", url).attr("title", value.name).text(value.name).appendTo(li);

            });
            scrollMoudle.showlist();//开始轮播
        },
        parseRank: function (data) {
            rank_ul.empty();//清空元素内容物
            $.each(data, function (n, value) {
                var url = "/comic?comicid=" + value.comic_id;

                var li = $("<li></li>").appendTo(rank_ul);//li
                switch (n) {
                    case 0:
                        li.addClass("q first");
                        break;
                    case 1:
                        li.addClass("q second");
                        break;
                    case 2:
                        li.addClass("q third");
                        break;
                }

                var index = $('<i></i>').text(n + 1).appendTo(li);//序号
                var a_name = $('<a  target="_blank" class="name" ></a>').attr("href", url).text(value.comic_name).appendTo(li);//名字
                var em_count = $('<em></em>').text(value.count).appendTo(li);//收藏数
            })
        }
    };

    /**
     * 负责获得排名
     * @type {{getRank: rankMoudle.getRank}}
     */
    var rankMoudle = {
        getRank: function () {
            g_myAjax("/rank", {}, function (data) {
                parseMoudle.parseRank(data)
            });
        }
    };

    //一开始请求list
    var data = {};
    myAjax(data, parseMoudle.parseComic);
    //请求排行榜数据
    rankMoudle.getRank();
});
