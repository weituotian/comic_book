<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:include page="../include/top_container.jsp"/>

<%--主要table--%>
<table>
    <tr>
        <td>用户名:</td>
        <td>${sessionScope.user.user_name}</td>
    </tr>
    <tr>
        <td>邮箱:</td>
        <td>${sessionScope.user.user_mail}</td>
        <td id="mail_vertify">${sessionScope.user.user_vertify}</td>
    </tr>
    <tr>
        <td>密码</td>
        <td><span>*********</span></td>
        <td><a id="change_password">修改密码</a></td>
    </tr>
    <tr>
        <td>头像:</td>
        <td><img src="" id="img_face_big"
                 style="width:180px;margin-top:4px;border-radius:180px;box-shadow:0 0 12px #7E7E7E;"/></td>
        <td><a id="change_avatar" href="${pageContext.request.contextPath}/home/face.jsp">修改头像</a></td>
    </tr>
</table>


<%--修改密码的css--%>
<style type="text/css">

    /*stylesheet 1/4 | https://static-s.bilibili.com/css/core-v5/base.css*/
    input, select {
        outline: 0;
    }

    ul, li {
        margin: 0;
        padding: 0;
        list-style-type: none;
    }

    em, i {
        font-style: normal;
        font-weight: normal;
    }

    p, span {
        margin: 0;
        padding: 0;
    }

    /*stylesheet 2/4 | https://static-s.bilibili.com/passport/css/zclc2.1460959081.css*/
    input {
        border: 0;
    }

    [class^="control-btn-"] {
        -webkit-user-select: none;
    }

    li, ul {
        list-style: none;
    }

    div, input, li, p, span, ul {
        margin: 0;
        padding: 0;
    }

    [class*="control-btn-"], [class^="control-btn-"] {
        vertical-align: middle;
        display: inline-block;
        position: relative;
        cursor: pointer;
        overflow: hidden;
        height: 36px;
        line-height: 36px;
        width: 100%;
        color: #424242;
        outline: 0;
    }

    select {
        float: left;
    }

    select {
        height: 40px;
        width: 91px;
        border: 1px solid #dfdfdf;
        border-radius: 3px;
    }

    .sbi-l, .sbi-m {
        float: left;
    }

    .sr-b li {
        font-size: 12px;
    }

    .sr-b li {
        height: auto;
        margin-bottom: 22px;
    }

    .sb-info {
        position: relative;
        height: 28px;
        line-height: 28px;
        color: #666;
        clear: both;
    }

    .sbi-l {
        width: 95px;
        text-align: right;
        margin-right: 23px;
    }

    .sbi-m input {
        width: 218px;
        height: 36px;
        border: 1px solid #ddd;
        border-radius: 2px;
        line-height: 36px;
        padding-left: 5px;
        margin-right: 10px;
        margin-left: -4px;
    }

    .e-drop {
        width: 500px;
        height: 30px;
        line-height: 30px;
        border: 1px solid #ddd;
        margin-top: 7px;
        position: relative;
        padding: 8px 0;
        color: grey;
        display: none;
    }

    .arrow-wrap {
        position: absolute;
        left: 60px;
        top: -13px;
        width: 13px;
        height: 7px;
    }

    .arrow-wrap em, .arrow-wrap i {
        position: absolute;
        display: block;
        width: 0px;
        height: 0px;
        overflow: hidden;
        border-width: 6px;
        border-style: dashed dashed solid;
        top: 0px;
        left: 0px;
    }

    .arrow-wrap em {
        border-color: transparent transparent #ddd;
    }

    .arrow-wrap i {
        border-color: transparent transparent #fff;
        top: 2px;
        z-index: 2;
    }

    .sure {
        color: #fff;
        font-weight: 700;
        text-align: center;
        cursor: pointer;
        width: 54px !important;
        height: 28px !important;
        background-color: #00a0d8;
    }

    .sure {
        line-height: 28px !important;
        border-radius: 2px !important;
    }

    .c-drop {
        height: auto;
    }

    .c-drop .sure {
        margin-left: 118px;
        width: 58px;
        margin-right: 15px;
    }

    .c-drop li:last-child {
        margin-bottom: 0px;
    }

    .control-btn-select {
        border: 1px solid #ddd;
        border-radius: 2px;
    }

    .control-btn-select em {
        position: absolute;
        right: 15px;
        top: 50%;
        margin-top: -3px;
        width: 13px;
        height: 5px;
        background: url(https://static-s.bilibili.com/passport/img/icons04.png) -75px -80px no-repeat;
    }

    .m-secuerity {
        width: 170px;
    }

    input, li, p, select, ul {
        margin: 0px;
        padding: 0px;
        color: #717171;
    }

    input, select {
        font-style: normal;
        font-variant: normal;
        font-weight: normal;
        font-stretch: normal;
        font-size: 12px;
        line-height: 1.125;
        font-family: Arial, Helvetica, sans-serif;
    }

    i {
        font-style: normal;
    }

    [class^="control-btn-"] {
        vertical-align: middle;
        display: inline-block;
        position: relative;
        cursor: pointer;
        overflow: hidden;
    }

    [class^="control-btn-"] span {
        text-overflow: ellipsis;
        overflow: hidden;
        display: block;
        color: #fff;
        vertical-align: middle;
        text-align: center;
    }

    .control-btn-select select {
        position: absolute;
        opacity: 0;
        border: none;
        width: 100%;
        height: 100%;
        top: 0px;
        left: 0px;
        color: #424242;
        outline: 0px;
        cursor: pointer;
        background: 0px 0px;
    }

    .control-btn-select {
        height: 30px;
        line-height: 30px;
        width: 150px;
        background-position: 0px 0px;
    }

    .control-btn-select span {
        color: #45484d;
        padding: 0px 22px 0px 0px;
        margin-left: 10px;
        height: 30px;
        line-height: 30px;
        text-align: left;
        white-space: nowrap;
        background-position: 100% 0px;
    }

    .control-btn-select:hover {
        background-position: 0px -40px;
    }

    .control-btn-select:hover span {
        background-position: 100% -40px;
    }

    /*stylesheet 3/4 | https://static-s.bilibili.com/passport/seajs/plugin/dialog/skins/white.css*/

    /*stylesheet 4/4 | inline*/
</style>
<%--头像的修改--%>
<div class="e-drop c-drop" id="avatar-drop" style="display: none;">
    <div class="arrow-wrap">
        <em></em> <i></i>
    </div>

</div>
<%--修改密码的div--%>
<div class="e-drop c-drop" id="p-drop" style="display: none;">
    <div class="arrow-wrap">
        <em></em> <i></i>
    </div>
    <form id="update_form">
        <ul>
            <li>
                <div class="sb-info">
                    <p class="sbi-l">当前密码：</p>
                    <div class="sbi-m">
                        <input class="mob" id="oldpwd" name="oldpwd" type="password" placeholder="输入当前密码">
                    </div>
                </div>
            </li>
            <li>
                <div class="sb-info">
                    <p class="sbi-l">新的密码：</p>
                    <div class="sbi-m">
                        <input class="mob" id="newpwd" name="newpwd" type="password" placeholder="输入新的密码（6-16位字符组成）">
                    </div>
                </div>
            </li>
            <li>
                <div class="sb-info">
                    <p class="sbi-l">再次确认：</p>
                    <div class="sbi-m">
                        <input class="mob" id="newpwd2" name="newpwd2" type="password" placeholder="再次确认新的密码">
                    </div>
                </div>
            </li>
            <li>
                <div class="sb-info">
                    <input class="sure" id="btn_changePassword" type="button" value="确认">
                </div>
            </li>
        </ul>
    </form>
</div>
<script>
    $(function () {
        var change_password = $('#change_password');
        var p_drop = $('#p-drop');
        var change_avatar = $('#change_avatar');
        var avatar_drop = $('#avatar-drop');
        //弹出框内的控件
        var update_form = $('#update_form');
        var oldpwd = $('#oldpwd');
        var newpwd = $('#newpwd');
        var btn_changePassword = $('#btn_changePassword');
        //对话框
        var myDialog = $('#dialog-message');
        //检测是否登录
        if (!isLogin()) {
            window.location.href = "/login.jsp";
        }

        //邮箱是否验证
        var user_vertify = "${sessionScope.user.user_vertify}";
        var mail_vertify = $('#mail_vertify');
        if (user_vertify == "1") {
            mail_vertify.html("邮箱已经验证！");
        } else if (user_vertify == "0") {
            mail_vertify.html("<a href='${pageContext.request.contextPath}/mail?act=send'>发送邮箱验证</a>");
        }

        //头像显示
        var img_face = $('#img_face_big');
        img_face.attr("src", getFaceURL());

        change_password.click(function () {
            //弹出下拉框
            p_drop.toggle();
        });
        change_avatar.click(function () {
            //弹出下拉框
            avatar_drop.toggle();
        });
        btn_changePassword.click(function () {
            //提交请求
            update_form.submit();
        });

        //验证
        update_form.validate({
            //键盘弹起触发的函数
            onkeyup: function (element) {
                $(element).valid();
            },
            highlight: function (element, errorClass, validClass) {
                $(element).addClass("error").removeClass("valid");
                $(element).parent("div").find("span.ui-icon").addClass("ui-icon-circle-close").removeClass("ui-icon-circle-check");
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).removeClass("error").addClass("valid");
            },
            //提交按钮被点击后执行的函数,需要验证成功
            submitHandler: function (form) {
                //暂时禁用提交按钮
                btn_changePassword.attr("disabled", true);
                btn_changePassword.text("提交中……");
                toReg();
            },
            rules: {
                oldpwd: {
                    required: true
                },
                newpwd: {
                    required: true,
                    rangelength: [5, 20]
                },
                newpwd2: {
                    required: true,
                    rangelength: [5, 20]
                }
            },
            messages: {
                oldpwd: {
                    required: "为空？"
                },
                newpwd: {
                    required: "为空？",
                    rangelength: $.format("长度为{0}到{1}")
                },
                newpwd2: {
                    required: "为空？",
                    rangelength: $.format("长度为{0}到{1}")
                }
            },
            //设置错误信息存放标签
            errorElement: "em",
            //放置错误标签的位置
            errorPlacement: function (error, element) {
                element.after(error);
                error.before($("<span class='ui-icon' style='display: inline-block'></span>"));
                $(element).valid();//触发验证
            },
            success: function (label) {
                //字段验证成功后的方法
                label.parent("div").find("input").removeClass("error").addClass("valid");
                label.parent("div").find("span.ui-icon").addClass("ui-icon-circle-check").removeClass("ui-icon-circle-close");
                label.addClass("valid").text('ok');
            }
        });

        //ajax提交详细
        function toReg() {
            $.ajax({
                url: "/person",
                type: "POST",
                datatype: "json",
                timeout: 3000,
                data: {
                    oldpwd: oldpwd.val(),
                    newpwd: newpwd.val()
                },
                success: function (rdata, textStatus) {
                    var data = $.parseJSON(rdata);

                    if (data.error > 0) {
                        //有错误
                        myDialog.find("#icon").attr("class", "ui-icon ui-icon-circle-close");
                        btn_changePassword.attr("disabled", false);
                        btn_changePassword.text("重新提交");
                    } else {
                        //修改成功
                        myDialog.find("#icon").attr("class", "ui-icon ui-icon-circle-check");
                        //3秒后跳转到首页
                        setTimeout(function () {
                            window.location.href = g_contextPath + "/index.jsp";
                        }, 3000);
                        btn_changePassword.text("修改成功！");
                    }

                    //设置消息
                    myDialog.find('#msg').html(data.msg);
                    //打开对话框
                    myDialog.dialog("open");
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert(XMLHttpRequest);
                    alert(textStatus);
                    alert(errorThrown);
                }
            })
        }
    });
</script>
</body>
</html>
