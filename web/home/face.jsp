<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--<%@include file="../include/top_container.jsp" %>--%>
<jsp:include page="../include/top_container.jsp"/>

<link href="../css/face.css" rel="stylesheet"/>
<%--图片剪切的js--%>
<script type="text/javascript" src="../js/cropbox.js"></script>

<div style="text-align: center">
    <img src="" id="img_face_big" style="width:180px;margin-top:4px;border-radius:180px;box-shadow:0 0 12px #7E7E7E;"/>
</div>

<div class="container">
    <div class="imageBox">
        <div class="thumbBox"></div>
        <div class="spinner" style="display: none">Loading...</div>
    </div>
    <div class="action">
        <!-- <input type="file" id="file" style=" width: 200px">-->
        <div class="new-contentarea tc"><a href="javascript:void(0)" class="upload-img">
            <label for="upload-file" style="line-height: inherit;">选择图像</label>
        </a>
            <input type="file" class="" name="upload-file" id="upload-file"/>
        </div>
        <input type="button" id="btnCrop" class="Btnsty_peyton" value="裁切">
        <input type="button" id="btnZoomIn" class="Btnsty_peyton" value="+">
        <input type="button" id="btnZoomOut" class="Btnsty_peyton" value="-">
        <input type="button" id="upload" class="Btnsty_peyton" value="上传">
    </div>
    <div class="cropped" id="cropped"></div>
    <div class="progress-bar progress-bar-success" role="progressbar"
         style="width: 0;position: absolute;margin-top: 20px">
        <span class="sr-only">0% Complete</span>
    </div>
</div>

<%--用作调试的菜单--%>
<%--<div>--%>
<%--<form action="/face" method="post" enctype="multipart/form-data">--%>
<%--<input type="file" name="sss">--%>
<%--<input type="submit" value="tijiao">--%>
<%--</form>--%>
<%--</div>--%>

<script type="text/javascript">

    $(document).ready(function () {
        //检测是否登录
        if (!isLogin()) {
            window.location.href = "/login.jsp";
        }

        //头像显示
        var img_face = $('#img_face_big');
        img_face.attr("src", getFaceURL());

        //选项
        var options =
        {
            thumbBox: '.thumbBox',
            spinner: '.spinner',
            imgSrc: '../images/avatar.png'
        };
        //对话框
        var myDialog = $('#dialog-message');
        //剪切后的图片div容器
        var cropped = $('#cropped');
        var cropper = $('.imageBox').cropbox(options);
        //储存上传的图片数据
        var img;
        var thePic;
        //当input选择文件的时候，读取图片数据
        $('#upload-file').on('change', function () {
            //alert("what?");
            var reader = new FileReader();
            thePic = this.files[0];
            if (thePic) {
                var ext = this.files[0].name.toLowerCase().split('.').pop();
                if ($.inArray(ext, extList) >= 0) {
                    //开始读取文件
                    reader.readAsDataURL(thePic);
                } else {
                    //设置消息
                    myDialog.find('#msg').html("文件格式不对！");
                    //打开对话框
                    myDialog.dialog("open");
                }
            }

            reader.onload = function (e) {
                //文件读取成功完成时触发
                cropper.image.src = e.target.result;
            };
            //this.files = [];
        });
        //裁剪按钮被点击
        $('#btnCrop').on('click', function () {
            img = cropper.getDataURL();
            cropped.html('');
            cropped.append('<img src="' + img + '" align="absmiddle" style="width:64px;margin-top:4px;border-radius:64px;box-shadow:0 0 12px #7E7E7E;" ><p>64px*64px</p>');
            cropped.append('<img src="' + img + '" align="absmiddle" style="width:128px;margin-top:4px;border-radius:128px;box-shadow:0 0 12px #7E7E7E;"><p>128px*128px</p>');
            cropped.append('<img src="' + img + '" align="absmiddle" style="width:180px;margin-top:4px;border-radius:180px;box-shadow:0 0 12px #7E7E7E;"><p>180px*180px</p>');
        });
        //放大
        $('#btnZoomIn').on('click', function () {
            cropper.zoomIn();
        });
        //缩小
        $('#btnZoomOut').on('click', function () {
            cropper.zoomOut();
        });

        var url = "/face";//文件上传的url:http://localhost/clouddoc/upload.php
        var extFilter = 'jpg;png;gif';//文件后缀名匹配
        var extList = extFilter.toLowerCase().split(';');//文件后缀名
        var btn_upload = $('#upload');//上传按钮
        //更新进度条
        var update_progress = function (id, percent) {
            var percentStr = percent + '%';
            $('div.progress-bar').width(percent + "%");
            $('span.sr-only').html(percentStr + ' Complete');
            console.log("progress：" + percentStr);
        };
        //上传按钮被点击
        btn_upload.click(function () {
            $('#btnCrop').click();
            //创建新表单数据
            var fd = new FormData();
            //将新文件加入表单数据,第三个参数是文件名字
            fd.append("files", cropper.getBlob(), thePic.name);
            // Ajax Submit
            //http://www.w3school.com.cn/jquery/ajax_ajax.asp
            $.ajax({
                url: url,
                type: "POST",
                dataType: "json",
                data: fd,
                cache: false,
                contentType: false,
                processData: false,
                forceSync: false,
                xhr: function () {
                    var xhrobj = $.ajaxSettings.xhr();
                    if (xhrobj.upload) {
                        xhrobj.upload.addEventListener('progress', function (event) {
                            var percent = 0;
                            var position = event.loaded || event.position;
                            var total = event.total || event.totalSize;
                            if (event.lengthComputable) {
                                percent = Math.ceil(position / total * 100);
                            }
                            update_progress(0, percent);
                        }, false);
                    }

                    return xhrobj;
                },
                success: function (data, message, xhr) {
                    myDialog.find("#msg").html(data.msg);
                    myDialog.dialog("open");
                    setTimeout(function () {
                        var url = data.url + "?t=" + Math.random();
                        img_face.attr("src", url);
                        $('#img_face').attr("src", url);//顺便也更新下导航条的头像url
                    }, 1000);
                },
                error: function (xhr, status, errMsg) {
                    alert("上传错误！");
                },
                complete: function (xhr, textStatus) {

                    //一个文件提交完ajax就继续下一个文件
                    //widget.processQueue();
                }
            });
        });
    });
</script>

</body>
</html>
