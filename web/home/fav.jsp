<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:include page="../include/top_container.jsp"/>

<link rel="stylesheet" href="../css/fav.css">


<%--操作按钮--%>
<div id="multi-action" class="clearfix">
    <span class="text">已选<span id="multi-count">0</span>个视频</span>
    <span class="button host" id="multi-delete">删除</span>
    <span class="button" id="multi-cancel">取消选择</span>
</div>

<%--list显示区域--%>
<div class="content clearfix" id="list_container">

    <!-- 简略列表 -->
    <div class="small-item" data-aid="2316425">
        <a href="http://www.bilibili.com/video/av2316425/" target="_blank" class="cover">
            <img src="http://i2.hdslb.com/160_100/video/d7/d7dfb9a76bb02ffd3ef6145affda1a96.jpg"
                 alt="我觉得《天体的秩序》和《灰色的乐园》的ED还可以这样换">
            <span class="length">3:31</span>
            <div class="disabled-cover">视频已失效</div>
        </a>
        <a href="http://www.bilibili.com/video/av2316425/" target="_blank" class="title">我觉得《天体的秩序》和《灰色的乐园》的ED还可以这样换</a>
        <div class="meta">
            <span class="play"><span class="icon"></span>443</span>
            <span class="comments"><span class="icon"></span>7</span>
        </div>
        <div class="meta pubdate">收藏于 04-30</div>
        <span class="checkbox">
            <span class="icon" title="选中这个视频"></span>
        </span>
    </div>


    <!-- 简略列表 -->
    <div class="small-item" data-aid="3239560">
        <a href="http://www.bilibili.com/video/av3239560/" target="_blank" class="cover">
            <img src="http://i0.hdslb.com/160_100/video/a8/a8fe8dafaa1ed247d52c4e1b9ae808df.jpg"
                 alt="2015新番动画中的相似镜头提纯 · 合">
            <span class="length">10:36</span>
            <div class="disabled-cover">视频已失效</div>
        </a>
        <a href="http://www.bilibili.com/video/av3239560/" target="_blank" class="title">2015新番动画中的相似镜头提纯 · 合</a>
        <div class="meta">
            <span class="play"><span class="icon"></span>42.6万</span>
            <span class="comments"><span class="icon"></span>4949</span>
        </div>
        <div class="meta pubdate">收藏于 04-07</div>
        <span class="checkbox"><span class="icon" title="选中这个视频"></span></span>
    </div>


    <!-- 简略列表 -->
    <div class="small-item" data-aid="615027">
        <a href="http://www.bilibili.com/video/av615027/" target="_blank" class="cover">
            <img src="http://i2.hdslb.com/160_100/u_f/80577effc5043e4b43ed420814f86436.jpg" alt="【MMD教程】PMX編集表情向教程">
            <span class="length">12:27</span>
            <div class="disabled-cover">视频已失效</div>
        </a>
        <a href="http://www.bilibili.com/video/av615027/" target="_blank" class="title">【MMD教程】PMX編集表情向教程</a>
        <div class="meta">
            <span class="play"><span class="icon"></span>3047</span>
            <span class="comments"><span class="icon"></span>45</span>
        </div>
        <div class="meta pubdate">收藏于 03-28</div>
        <span class="checkbox"><span class="icon" title="选中这个视频"></span></span>
    </div>


    <!-- 简略列表 -->
    <div class="small-item" data-aid="4193905">
        <a href="http://www.bilibili.com/video/av4193905/" target="_blank" class="cover">
            <img src="http://i0.hdslb.com/bfs/archive/799e316e37fc96380ddf5a84f564860a914de1fb.jpg_160x100.jpg"
                 alt="[真三MMD]真·牛郎团的MR TAXI">
            <span class="length">0:36</span>
            <div class="disabled-cover">视频已失效</div>
        </a>
        <a href="http://www.bilibili.com/video/av4193905/" target="_blank" class="title">[真三MMD]真·牛郎团的MR TAXI</a>
        <div class="meta">
            <span class="play"><span class="icon"></span>1302</span>
            <span class="comments"><span class="icon"></span>59</span>
        </div>
        <div class="meta pubdate">收藏于 03-26</div>
        <span class="checkbox"><span class="icon" title="选中这个视频"></span></span>
    </div>


    <!-- 简略列表 -->
    <div class="small-item" data-aid="4129436">
        <a href="http://www.bilibili.com/video/av4129436/" target="_blank" class="cover">
            <img src="http://i0.hdslb.com/bfs/archive/e4acdb843b4b7e2acb5a6fe3a23506e0c10c4842.jpg_160x100.jpg"
                 alt="【银魂/进击/鬼澈/NR/虫师】confident死鱼眼组【镜头配布】">
            <span class="length">1:32</span>
            <div class="disabled-cover">视频已失效</div>
        </a>
        <a href="http://www.bilibili.com/video/av4129436/" target="_blank" class="title">【银魂/进击/鬼澈/NR/虫师】confident死鱼眼组【镜头配布】</a>
        <div class="meta">
            <span class="play"><span class="icon"></span>804</span>
            <span class="comments"><span class="icon"></span>58</span>
        </div>
        <div class="meta pubdate">收藏于 03-19</div>
        <span class="checkbox"><span class="icon" title="选中这个视频"></span></span>
    </div>


    <!-- 简略列表 -->
    <div class="small-item" data-aid="754590">
        <a href="http://www.bilibili.com/video/av754590/" target="_blank" class="cover">
            <img src="http://i1.hdslb.com/160_100/u_f/0f88578ac22be803b3ec640ca43d6704.jpg"
                 alt="【战国MMD】筆頭 Departures~为你而唱的爱之歌~">
            <span class="length">2:16</span>
            <div class="disabled-cover">视频已失效</div>
        </a>
        <a href="http://www.bilibili.com/video/av754590/" target="_blank" class="title">【战国MMD】筆頭
            Departures~为你而唱的爱之歌~</a>
        <div class="meta">
            <span class="play"><span class="icon"></span>209</span>
            <span class="comments"><span class="icon"></span>5</span>
        </div>
        <div class="meta pubdate">收藏于 03-12</div>
        <span class="checkbox"><span class="icon" title="选中这个视频"></span></span>
    </div>


    <!-- 简略列表 -->
    <div class="small-item" data-aid="3895600">
        <a href="http://www.bilibili.com/video/av3895600/" target="_blank" class="cover">
            <img src="http://i0.hdslb.com/160_100/video/c1/c11ea09edabd0d8607547d5c9f0b1593.jpg"
                 alt="【第16回MMD杯EX】KAZEHIME-The Wind Princess-Departure-【Fullver】">
            <span class="length">4:03</span>
            <div class="disabled-cover">视频已失效</div>
        </a>
        <a href="http://www.bilibili.com/video/av3895600/" target="_blank" class="title">【第16回MMD杯EX】KAZEHIME-The Wind
            Princess-Departure-【Fullver】</a>
        <div class="meta">
            <span class="play"><span class="icon"></span>5367</span>
            <span class="comments"><span class="icon"></span>79</span>
        </div>
        <div class="meta pubdate">收藏于 03-12</div>
        <span class="checkbox"><span class="icon" title="选中这个视频"></span></span>
    </div>


    <!-- 简略列表 -->
    <div class="small-item" data-aid="3472948">
        <a href="http://www.bilibili.com/video/av3472948/" target="_blank" class="cover">
            <img src="http://i2.hdslb.com/160_100/video/00/009637b79e9d220e5dcd4d03eda5ab65.jpg"
                 alt="【一拳MMD】琦玉老师的指尖舞蹈【kill everybody】">
            <span class="length">2:20</span>
            <div class="disabled-cover">视频已失效</div>
        </a>
        <a href="http://www.bilibili.com/video/av3472948/" target="_blank" class="title">【一拳MMD】琦玉老师的指尖舞蹈【kill
            everybody】</a>
        <div class="meta">
            <span class="play"><span class="icon"></span>590</span>
            <span class="comments"><span class="icon"></span>29</span>
        </div>
        <div class="meta pubdate">收藏于 02-18</div>
        <span class="checkbox"><span class="icon" title="选中这个视频"></span></span>
    </div>


    <!-- 简略列表 -->
    <div class="small-item" data-aid="3801205">
        <a href="http://www.bilibili.com/video/av3801205/" target="_blank" class="cover">
            <img src="http://i2.hdslb.com/160_100/video/5a/5afe4bcc0f5b00b9913b8cf497f4dbe8.jpg"
                 alt="周杰伦和二三次元的朋友在广药跳MJ的Dangerous">
            <span class="length">5:00</span>
            <div class="disabled-cover">视频已失效</div>
        </a>
        <a href="http://www.bilibili.com/video/av3801205/" target="_blank" class="title">周杰伦和二三次元的朋友在广药跳MJ的Dangerous</a>
        <div class="meta">
            <span class="play"><span class="icon"></span>782</span>
            <span class="comments"><span class="icon"></span>33</span>
        </div>
        <div class="meta pubdate">收藏于 02-10</div>
        <span class="checkbox"><span class="icon" title="选中这个视频"></span></span>
    </div>


    <!-- 简略列表 -->
    <div class="small-item" data-aid="3555482">
        <a href="http://www.bilibili.com/video/av3555482/" target="_blank" class="cover">
            <img src="http://i1.hdslb.com/160_100/video/2a/2a6a0a976f7a95c2226f19dacc06ecdd.jpg" alt="【超电磁炮MMD】炮姐的可爱颂">
            <span class="length">0:42</span>
            <div class="disabled-cover">视频已失效</div>
        </a>
        <a href="http://www.bilibili.com/video/av3555482/" target="_blank" class="title">【超电磁炮MMD】炮姐的可爱颂</a>
        <div class="meta">
            <span class="play"><span class="icon"></span>335</span>
            <span class="comments"><span class="icon"></span>3</span>
        </div>
        <div class="meta pubdate">收藏于 01-10</div>
        <span class="checkbox"><span class="icon" title="选中这个视频"></span></span>
    </div>


    <!-- 简略列表 -->
    <div class="small-item" data-aid="2807136">
        <a href="http://www.bilibili.com/video/av2807136/" target="_blank" class="cover">
            <img src="http://i1.hdslb.com/160_100/video/c2/c2fe97a6cdf2c936602f420c05e5eb8d.jpg"
                 alt="【2015MMD剧情向】MMD健美操大赛[剧情还原+动作配布向]">
            <span class="length">5:51</span>
            <div class="disabled-cover">视频已失效</div>
        </a>
        <a href="http://www.bilibili.com/video/av2807136/" target="_blank" class="title">【2015MMD剧情向】MMD健美操大赛[剧情还原+动作配布向]</a>
        <div class="meta">
            <span class="play"><span class="icon"></span>30.3万</span>
            <span class="comments"><span class="icon"></span>7750</span>
        </div>
        <div class="meta pubdate">收藏于 2015-11-30</div>
        <span class="checkbox"><span class="icon" title="选中这个视频"></span></span>
    </div>


    <!-- 简略列表 -->
    <div class="small-item" data-aid="3253475">
        <a href="http://www.bilibili.com/video/av3253475/" target="_blank" class="cover">
            <img src="http://i0.hdslb.com/160_100/video/31/3159b7e8e3f9aead097396b6efdf99f6.jpg"
                 alt="MMD Happy birthday Haku！">
            <span class="length">3:59</span>
            <div class="disabled-cover">视频已失效</div>
        </a>
        <a href="http://www.bilibili.com/video/av3253475/" target="_blank" class="title">MMD Happy birthday Haku！</a>
        <div class="meta">
            <span class="play"><span class="icon"></span>674</span>
            <span class="comments"><span class="icon"></span>50</span>
        </div>
        <div class="meta pubdate">收藏于 2015-11-21</div>
        <span class="checkbox"><span class="icon" title="选中这个视频"></span></span>
    </div>


    <!-- 简略列表 -->
    <div class="small-item" data-aid="2701732">
        <a href="http://www.bilibili.com/video/av2701732/" target="_blank" class="cover">
            <img src="http://i2.hdslb.com/160_100/video/ef/ef2931268f5b9f327f6261a54b1abc93.jpg" alt="【鬼畜】 【MMD】千本♂兄贵">
            <span class="length">4:28</span>
            <div class="disabled-cover">视频已失效</div>
        </a>
        <a href="http://www.bilibili.com/video/av2701732/" target="_blank" class="title">【鬼畜】 【MMD】千本♂兄贵</a>
        <div class="meta">
            <span class="play"><span class="icon"></span>906</span>
            <span class="comments"><span class="icon"></span>27</span>
        </div>
        <div class="meta pubdate">收藏于 2015-11-02</div>
        <span class="checkbox"><span class="icon" title="选中这个视频"></span></span>
    </div>


    <!-- 简略列表 -->
    <div class="small-item" data-aid="2821157">
        <a href="http://www.bilibili.com/video/av2821157/" target="_blank" class="cover">
            <img src="http://i0.hdslb.com/160_100/video/eb/eb6a6fa1c7038720862e587b7dfceb1b.jpg"
                 alt="MMD   →MJ（Gentleman）">
            <span class="length">5:33</span>
            <div class="disabled-cover">视频已失效</div>
        </a>
        <a href="http://www.bilibili.com/video/av2821157/" target="_blank" class="title">MMD →MJ（Gentleman）</a>
        <div class="meta">
            <span class="play"><span class="icon"></span>417</span>
            <span class="comments"><span class="icon"></span>8</span>
        </div>
        <div class="meta pubdate">收藏于 2015-10-28</div>
        <span class="checkbox"><span class="icon" title="选中这个视频"></span></span>
    </div>


    <!-- 简略列表 -->
    <div class="small-item" data-aid="778081">
        <a href="http://www.bilibili.com/video/av778081/" target="_blank" class="cover">
            <img src="http://i1.hdslb.com/160_100/u_f/56bf0178c5439558429006a75e8dc733.jpg" alt="【中文配音】某科学的超电磁炮S 第一话">
            <span class="length">41:10</span>
            <div class="disabled-cover">视频已失效</div>
        </a>
        <a href="http://www.bilibili.com/video/av778081/" target="_blank" class="title">【中文配音】某科学的超电磁炮S 第一话</a>
        <div class="meta">
            <span class="play"><span class="icon"></span>38.4万</span>
            <span class="comments"><span class="icon"></span>2.8万</span>
        </div>
        <div class="meta pubdate">收藏于 2013-11-23</div>
        <span class="checkbox"><span class="icon" title="选中这个视频"></span></span>
    </div>


    <!-- 简略列表 -->
    <div class="small-item" data-aid="837611">
        <a href="http://www.bilibili.com/video/av837611/" target="_blank" class="cover">
            <img src="http://i2.hdslb.com/160_100/u_f/a9a460271acbb717941b97dfcc70809e.jpg"
                 alt="【MAD】某科学的超电磁炮 -Hesitation Snow-【妹達編】">
            <span class="length">9:30</span>
            <div class="disabled-cover">视频已失效</div>
        </a>
        <a href="http://www.bilibili.com/video/av837611/" target="_blank" class="title">【MAD】某科学的超电磁炮 -Hesitation
            Snow-【妹達編】</a>
        <div class="meta">
            <span class="play"><span class="icon"></span>36.7万</span>
            <span class="comments"><span class="icon"></span>9439</span>
        </div>
        <div class="meta pubdate">收藏于 2013-11-17</div>
        <span class="checkbox"><span class="icon" title="选中这个视频"></span></span>
    </div>

</div>

<script type="text/javascript">
    $(document).ready(function () {
        //操作按钮
        var multiaction = $('#multi-action');//总div
        var multicount = $('#multi-count');//显示选择数量
        var multicancel = $('#multi-cancel');//取消按钮
        var multidelete = $('#multi-delete');//发起删除按钮
        //list容器
        var div_list_container = $('#list_container');

        multicancel.click(function () {//取消选中的
            div_list_container.find(".selected").removeClass("selected");
            multicount.text(0);
        });

        multidelete.click(function () {//发起删除请求
            var arr_comicId = "";
            div_list_container.find(".selected").each(function () {
                arr_comicId += $(this).attr("data-cid") + ",";
            });//连接起所有漫画id，逗号分割
            arr_comicId = arr_comicId.substr(0, arr_comicId.length - 1);//去除最后一个逗号
            var data = {
                user_name: g_user_name,
                comic_id: arr_comicId,
                type: "delete_multi"
            };
            console.log("data:" + arr_comicId);
            g_myAjax("/collect", data, function (data) {
                openDialog(data.msg, multidelete);//打开对话框
                if (data.result) {
                    div_list_container.find(".selected").slideUp();//隐藏删除了的元素
                }
                //collectMoudle.showlist();//刷新收藏夹
            });
        });

        //解析模块
        var parseMoudle = {
            parseComic: function (list) {
                div_list_container.empty();

                if (list.length > 0) {
                    multiaction.slideDown();
                }

                $.each(list, function (n, value) {
                    //遍历list
                    var comic = value.comic;
                    var url = g_contextPath + "/comic?comicid=" + comic.id;
                    //总div
                    var div = $('<div class="small-item"></div>').attr("data-cid", comic.id).appendTo(div_list_container);
                    //图像部分
                    var a1 = $('<a target="_blank" class="cover">').attr("href", url).appendTo(div);
                    var img = $('<img />').attr("src", comic.cover).appendTo(a1);
                    //名字
                    var a_title = $('<a target="_blank" class="title"></a>').text(comic.name).attr("href", url).appendTo(div);
                    //时间
                    var time = $('<div class="meta pubdate"></div>').text("收藏于" + value.time).appendTo(div);
                    //选中框架
                    var check = $('<span class="checkbox"><span class="icon" title="选中这个视频"></span></span>').appendTo(div);

                    //checkbox被选中
                    check.click(function () {
                        div.toggleClass("selected");
                        multicount.text(div_list_container.find(".selected").length);
                    });

                })
            }
        };

        //收藏模块
        var collectMoudle = {
            showlist: function () {
                var url = "/collect";
                var data = {
                    type: "list"
                };
                g_myAjax(url, data, function (data) {
                    //回调函数
                    parseMoudle.parseComic(data);
                    console.log(data);
                });
            }
        };

        if (!isLogin()) {
            //如果没有登录
            openDialog("登录后可以查看收藏哦！", multidelete);//打开对话框
            //window.location.href = g_contextPath + "/login.jsp";
        } else {
            collectMoudle.showlist();
        }

    })
</script>
</body>
</html>
