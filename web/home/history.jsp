<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:include page="../include/top_container.jsp"/>

<link rel="stylesheet" href="../css/history.css">


<%--同步按钮css--%>
<style>
    .button {
        border: 1px solid #99a2aa;
        border-radius: 4px;
        cursor: pointer;
        display: inline-block;
        line-height: 24px;
        padding: 0 10px;
        margin-left: 5px;
    }

    .button:hover {
        border-color: #00a1d6;
        color: #00a1d6;
    }
</style>


<%--主要list--%>
<div class="container-body">
    <div class="newlist_info">
        <div class="b-head"><i class="b-icon b-icon-history"></i><span class="b-head-t" id="his_tip">历史观看列表</span><span
                class="button host" id="sync" style="display: none">同步历史记录</span></div>
    </div>
    <div class="new_list history">
        <ul class="history-list" id="history_list">
            <li class="history-record">
                <div class="l-info">
                    <div class="lastplay-time">
                        <i class="history-red-round"></i>
                        <span class="lastplay-d">今天</span>
                        <span class="lastplay-t">18:14</span>
                    </div>
                </div>
                <div class="r-info">
                    <a href="//www.bilibili.com/video/av4699708" target="_blank" class="preview">
                        <img src="http://i2.hdslb.com/bfs/archive/b146261d703af41eff2478b15ce075eb6540fa9b.jpg_320x200.jpg">
                    </a>
                    <a class="typename" href="/newlist.html?typeid=27">(综合)</a>
                    <a href="//www.bilibili.com/video/av4699708" target="_blank" class="title">【YB素材】决裂的IF酱</a>
                    <div class="w-info">
                        <i class="play b-icon b-icon-v-play" title="观看">3</i>
                        <i class="dm b-icon b-icon-v-dm" title="弹幕">0</i>
                        <i class="fav b-icon b-icon-v-fav" title="收藏">0</i>
                        <i class="coin b-icon b-icon-v-coin" title="硬币">0</i>
                    </div>
                    <i class="history-delete b-icon b-icon-delete">删除</i>
                </div>
            </li>
            <li class="history-record">
                <div class="l-info">
                    <div class="lastplay-time"><i class="history-red-round"></i><span class="lastplay-d">今天</span><span
                            class="lastplay-t">18:14</span></div>
                </div>
                <div class="r-info"><a href="//www.bilibili.com/video/av4699318" target="_blank" class="preview"><img
                        src="http://i1.hdslb.com/bfs/archive/640746bbd1fdd75365fa95358874f5bdb7ba86e6.jpg_320x200.jpg"><span
                        class="lasting-time">1:54</span></a><a class="typename"
                                                               href="/newlist.html?typeid=24">(MAD·AMV)</a><a
                        href="//www.bilibili.com/video/av4699318" target="_blank" class="title">【海王星MAD】True My
                    Heart</a>
                    <div class="w-info"><i class="play b-icon b-icon-v-play" title="观看">20</i><i
                            class="dm b-icon b-icon-v-dm" title="弹幕">0</i><i class="fav b-icon b-icon-v-fav" title="收藏">1</i><i
                            class="coin b-icon b-icon-v-coin" title="硬币">1</i></div>
                    <i class="history-delete b-icon b-icon-delete">删除</i></div>
            </li>
            <li class="history-record">
                <div class="l-info">
                    <div class="lastplay-time"><i class="history-red-round"></i><span class="lastplay-d">今天</span><span
                            class="lastplay-t">18:14</span></div>
                </div>
                <div class="r-info"><a href="//www.bilibili.com/video/av3674441" target="_blank" class="preview"><img
                        src="http://i2.hdslb.com/320_200/video/f7/f7d3eb486cd816fce2866954b3e37b91.jpg"><span
                        class="lasting-time">17:32</span></a><a class="typename"
                                                                href="/newlist.html?typeid=17">(单机联机)</a><a
                        href="//www.bilibili.com/video/av3674441" target="_blank" class="title">【死神VS火影】朽木白哉通关</a>
                    <div class="w-info"><i class="play b-icon b-icon-v-play" title="观看">540</i><i
                            class="dm b-icon b-icon-v-dm" title="弹幕">0</i><i class="fav b-icon b-icon-v-fav" title="收藏">0</i><i
                            class="coin b-icon b-icon-v-coin" title="硬币">0</i></div>
                    <i class="history-delete b-icon b-icon-delete">删除</i></div>
            </li>
            <li class="history-record">
                <div class="l-info">
                    <div class="lastplay-time"><i class="history-red-round"></i><span class="lastplay-d">今天</span><span
                            class="lastplay-t">01:11</span></div>
                </div>
                <div class="r-info"><a href="//www.bilibili.com/video/av161281" target="_blank" class="preview"><img
                        src="http://i1.hdslb.com/320_200/user/617/61709/13193411541a69f87b05ea7742.jpg"><span
                        class="lasting-time">23:56</span></a><a class="typename"
                                                                href="/newlist.html?typeid=33">(连载动画)</a><a
                        href="//www.bilibili.com/video/av161281" target="_blank" class="title">【10月】请认真和我恋爱 04 【幻樱】</a>
                    <div class="w-info"><i class="play b-icon b-icon-v-play" title="观看">0</i><i
                            class="dm b-icon b-icon-v-dm" title="弹幕">0</i><i class="fav b-icon b-icon-v-fav" title="收藏">0</i><i
                            class="coin b-icon b-icon-v-coin" title="硬币">0</i></div>
                    <i class="history-delete b-icon b-icon-delete">删除</i></div>
            </li>
        </ul>
    </div>
</div>

<script type="text/javascript">

    /**
     * 时间类型的格式化函数
     */
    Date.prototype.format = function (fmt) { //author: meizz
        var o = {
            "M+": this.getMonth() + 1,                 //月份
            "d+": this.getDate(),                    //日
            "h+": this.getHours(),                   //小时
            "m+": this.getMinutes(),                 //分
            "s+": this.getSeconds(),                 //秒
            "q+": Math.floor((this.getMonth() + 3) / 3), //季度
            "S": this.getMilliseconds()             //毫秒
        };
        if (/(y+)/.test(fmt))
            fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
        for (var k in o)
            if (new RegExp("(" + k + ")").test(fmt))
                fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
        return fmt;
    };

    /**
     * 获取服务器的时间
     * @param option
     */
    function getNowTime(option) {
        var xhr = null;
        if (window.XMLHttpRequest) {
            xhr = new window.XMLHttpRequest();
        } else { // ie
            xhr = new ActiveObject("Microsoft");
        }
        // 通过get的方式请求当前文件
        xhr.open("get", "/");
        xhr.send(null);
        // 监听请求状态变化
        xhr.onreadystatechange = function () {
            var time = null,
                    curDate = null;
            if (xhr.readyState === 2) {
                // 获取响应头里的时间戳
                time = xhr.getResponseHeader("Date");
                console.log(xhr.getAllResponseHeaders());
                curDate = new Date(time);
                console.log("服务器时间是：" + curDate.getFullYear() + "-" + (curDate.getMonth() + 1) + "-" + curDate.getDate() + " " + curDate.getHours() + ":" + curDate.getMinutes() + ":" + curDate.getSeconds());
            }
        }
    }

    /**
     * 获取今天 昨天 或具体日期
     * @param d
     * @returns {string}
     */
    function getFriendlyDate(d) {
        var a = new Date;
        a.setHours(0);
        a.setMinutes(0);
        a.setSeconds(0);
        a.setMilliseconds(0);
        var date = new Date(d);
        return date.getMilliseconds() > a.getMilliseconds() ? "今天" : ( 86400000 > a - d ? "昨天" : (new Date(d)).format("MM-dd"))
    }


    $(document).ready(function () {
        var ul = $('#history_list');
        var his_tip = $('#his_tip');//历史记录提示标题

        //getNowTime();

        var parseMoudle = {
            parseList: function (data) {
                ul.empty();//先清空ul
                his_tip.text("历史记录列表（" + data.length + ")条");
                //遍历每一个历史记录
                $.each(data, function (n, history) {
                    if (!history) {
                        return;//history为空直接跳过
                    }

                    var li = $("<li class='history-record'></li>").appendTo(ul);//外部的li
                    //左右两边的div
                    var div_l = $('<div class="l-info"></div>').appendTo(li);
                    var div_r = $('<div class="r-info"></div>').appendTo(li);

                    //显示时间
                    var lastplay_time = $(
                            ['<div class="lastplay-time">',
                                '<i class="history-red-round"></i>',
                                '<span class="lastplay-d">' + (new Date(history.time)).format("MM-dd") + '</span>',
                                '<span class="lastplay-t">' + (new Date(history.time)).format("hh:mm") + '</span>',
                                '</div>'].join("")
                    ).appendTo(div_l);

                    //图像部分
                    var url = g_contextPath + "/comic?comicid=" + history.comic_id;
                    var face = $(
                            ['<a href="' + url + '" target="_blank" class="preview">',
                                '<img src="' + history.comic.cover + '">',
                                '</a>'].join("")
                    ).appendTo(div_r);

                    //名字部分
                    var a_type = $('<a class="typename" href="#">漫画</a>').appendTo(div_r);
                    var a_name = $('<a href="' + url + '" target="_blank" class="title"></a>').text(history.comic.name).appendTo(div_r);

                    //看到第几章部分
                    var text = "还没开始阅读……";
                    var chapter = history.chapter;
                    var cUrl = "#";
                    if (chapter) {
                        cUrl = g_contextPath + "/read?chapterid=" + chapter.id;
                        text = "看到【第" + chapter.comic_index + "话】【" + chapter.title + "】";
                    }
                    var a = $('<div class="w-info"><i class="play b-icon b-icon-v-play"><a href="' + cUrl + '">' + text + '</a></i></div>').appendTo(div_r);


                    //删除按钮
                    var deleteBtn = $('<i class="history-delete b-icon b-icon-delete">删除</i>').appendTo(div_r);
                    deleteBtn.click(function () {
                        var cid = history.comic_id;//漫画id
                        if (isLogin()) {
                            //去请求删除
                            g_myAjax("/history", {type: "delete", comicid: cid}, function (data) {
                                //回调函数
                                if (data.success) {//服务器返回成功删除
                                    li.slideUp();
                                }
                            })
                        } else {
                            historyMoudle.delete(cid);
                            li.slideUp();
                        }
                    });

                })
            }
        };

        //同步按钮
        var sync = $('#sync');
        var hCount = historyMoudle.getHistoryCount();
        if (isLogin() && hCount > 0) {//用户已经登录并且本地历史记录大于0
            sync.show();
            sync.text("上传本地浏览记录" + hCount + "条");
        }
        sync.click(function () {//同步按钮被点击
            g_myAjax("/history", {type: "sync"}, function (data) {
                //回调函数
                openDialog(data.msg, sync);
                //getHistory();
            })
        });

        //ajax获取history
        function getHistory() {
            g_myAjax("/history", {type: "get"}, function (data) {
                //回调函数
                parseMoudle.parseList(data);
            })
        }

        getHistory();
    })
</script>
</body>
</html>
