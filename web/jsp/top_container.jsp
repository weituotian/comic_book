<%--
  Created by IntelliJ IDEA.
  User: ange
  Date: 2016/4/21
  Time: 17:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--头部的样式--%>
<link rel="stylesheet" href="../css/top_container.css">
<%--主要div--%>
<div class="z_top_container">
    <div class="z_top b-header-blur">
        <%--头部背景--%>
        <div class="b-header-mask-wrp">
            <div class="b-header-mask-bg"
                 style="background-image: url(http://i0.hdslb.com/headers/256bf7bd4fbb7c00b921fc50b3d6f32f.png);"></div>
            <div class="b-header-mask"></div>
        </div>
        <div class="z_header">
            <%--导航--%>
            <div class="z_top_nav">
                <ul>
                    <li class="home">
                        <a class="i-link" href=""><span>主站</span></a>
                    </li>
                    <li class="yoo" hasframe="true">
                        <a class="i-link" target="_blank" href="" title="赞助">赞助<em
                                class="new"></em></a>
                    </li>
                </ul>
            </div>
            <%--个人工具箱--%>
            <div class="uns_box">
                <ul class="menu">
                    <%--头像--%>
                    <li id="i_menu_profile_btn" guest="no" class="u-i i_user" i_menu="#i_menu_profile"
                        style="display: list-item;">
                        <a class="i-link" href="" target="_blank">
                            <img class="i_face"
                                 src="http://i1.hdslb.com/bfs/face/8035a6b6aa80beb39f321b0da7484a29b5bd8d14.jpg">
                        </a>
                        <div id="i_menu_profile" class="i_menu" style="display: none;">
                            <div class="i_menu_bg_t"></div>
                            <div class="info clearfix">
                                <div class="uname"><b>韦驮天</b><a href="http://www.bilibili.com/html/help.html#k_1"
                                                                target="_blank" class="user-info-level l4"></a></div>
                                <a href="https://account.bilibili.com/site/coin" target="_blank">
                                    <div class="coin" title="硬币"><b class="b-back"></b><b class="b-icon"></b>
                                        <div class="outside">
                                            <div class="pre">139</div>
                                            <div class="cur">140<span class="reward-for-login">&nbsp;登录奖励</span></div>
                                        </div>
                                    </div>
                                </a><a class="phone verified" href="https://account.bilibili.com/" target="_blank"
                                       tips="已绑定" tips-pos="l" initialized="true"></a><a class="email verified"
                                                                                         href="https://account.bilibili.com/"
                                                                                         target="_blank" tips="已绑定"
                                                                                         tips-pos="l"
                                                                                         initialized="true"></a></div>
                            <div class="user-info l4" id="lv_info">
                                <div class="user-info-hd"><span class="t">等级</span></div>
                                <div class="points-wrp" id="exp_wrp"><a
                                        href="https://account.bilibili.com/site/record?type=exp" target="_blank">
                                    <div class="points-cnt">
                                        <div class="lt"></div>
                                        <div class="fill-el"></div>
                                        <div class="bar">
                                            <div class="points" style="width: 58%;"></div>
                                        </div>
                                        <div class="points-schedule"><span class="now-points">6323</span><span
                                                class="next-points">/10800</span></div>
                                    </div>
                                </a>
                                    <div class="user-info-desc" style="display: none;"><span class="arrow-left"></span>
                                        <div class="lv-row">作为<strong>LV4</strong>，你可以：</div>
                                        <div>1、发射个性弹幕（彩色、高级、顶部、底部）<br>2、参与视频互动（评论、添加tag）<br>3、投稿成为偶像<br>4、历史播放上限：200
                                        </div>
                                        <a class="help-link" href="http://www.bilibili.com/html/help.html#k"
                                           target="_blank">会员等级相关说明 &gt;</a></div>
                                </div>
                            </div>
                            <div class="member-menu-wrp">
                                <ul class="member-menu">
                                    <li><a href="https://account.bilibili.com/site/home" target="_blank"
                                           class="account"><i class="b-icon b-icon-p-account"></i>个人中心</a></li>
                                    <li><a href="http://member.bilibili.com/#video_manage" target="_blank"
                                           class="member"><i class="b-icon b-icon-p-member"></i>投稿管理</a></li>
                                    <li><a href="https://pay.bilibili.com/" target="_blank" class="wallet"><i
                                            class="b-icon b-icon-p-wallet"></i>我的钱包</a></li>
                                    <li><a href="http://live.bilibili.com/i" target="_blank" class="live"><i
                                            class="b-icon b-icon-p-live"></i>直播中心</a></li>
                                </ul>
                            </div>
                            <div class="member-bottom">
                                <a class="logout" href="https://account.bilibili.com/login?act=exit">退出</a>
                            </div>
                        </div>
                    </li>
                    <%--消息--%>
                    <li id="i_menu_community_msg_btn" guest="no" i_menu="community_msg" class="u-i"
                        style="display: list-item;">
                        <a class="i-link" href="http://message.bilibili.com" target="_blank">消息</a>
                        <div class="num" id="message_num_total" style="display: none;">0</div>
                        <div class="dyn_wnd" id="community_msg" style="top: 42px; left: -27px; display: none;">
                            <div class="dyn_arrow"></div>
                            <div class="dyn_menu">
                                <div class="menu">
                                    <ul>
                                        <li><a href="http://message.bilibili.com#reply" target="_blank"> <i
                                                class="icons2 icons-reply"></i> <span class="inner-type">回复我的</span>
                                            <span class="inner-num inner_notify_reply" id="reply_me"
                                                  style="display: none;">0</span> </a></li>
                                        <li><a href="http://message.bilibili.com#at" target="_blank"> <i
                                                class="icons2 icons-at"></i> <span class="inner-type">&nbsp;&nbsp;@我的&nbsp;&nbsp;</span>
                                            <span class="inner-num inner_notify_at" id="at_me"
                                                  style="display: none;">0</span> </a></li>
                                        <li><a href="http://message.bilibili.com#love" target="_blank"> <i
                                                class="icons2 icons-love"></i> <span class="inner-type">收到的赞</span>
                                            <span class="inner-num inner_notify_love" id="praise_me"
                                                  style="display: none;">0</span> </a></li>
                                        <li><a href="http://message.bilibili.com#system" target="_blank"> <i
                                                class="icons2 icons-system"></i> <span class="inner-type">系统通知</span>
                                            <span class="inner-num inner_notify_message" id="notify_me"
                                                  style="display: none;">0</span> </a></li>
                                        <li><a href="http://message.bilibili.com/#whisper" target="_blank"> <i
                                                class="icons2 icons-message"></i> <span class="inner-type">我的私信</span>
                                            <span class="inner-num inner_notify_message" id="chat_me"
                                                  style="display: none;">0</span> </a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div style="display:none;border:1px solid #f25d8e;background:#fff;position:absolute;border-radius:4px;line-height:36px;text-align:left;padding:0 15px;top:52px;left:-8px;white-space:nowrap;">
                            <div style="background: url(http://static.hdslb.com/images/base/icons.png) -841px -1101px no-repeat;width:38px;height:12px;position:absolute;top:-12px;"></div>
                            <ul id="floag_msg"></ul>
                        </div>
                    </li>
                    <%--动态--%>
                    <li id="i_menu_msg_btn" guest="no" i_menu="#dyn_wnd" class="u-i" style="display: list-item;">
                        <div class="num" id="dynamic_num_total"></div>
                        <a class="i-link" href="http://www.bilibili.com/account/dynamic" target="_blank">动态</a>
                        <div id="dyn_wnd" style="top: 42px; left: -153px;">
                            <div class="dyn_menu">
                                <div class="menu">
                                    <ul>
                                        <li mode="video" class="on">视频
                                            <div class="num" id="video_num"></div>
                                        </li>
                                        <li mode="live">直播
                                            <div class="num" id="live_num"></div>
                                        </li>
                                        <li mode="draw">画友
                                            <div class="num" id="draw_num"></div>
                                        </li>
                                    </ul>
                                    <div class="line"></div>
                                </div>
                            </div>
                            <div class="dyn_list_wrapper view-video" mode="video">
                                <ul class="dyn_list" mode="video">
                                    <li class="loading">loading...</li>
                                </ul>
                            </div>
                            <div class="dyn_list_wrapper view-live" mode="live" style="display: none;">
                                <ul class="dyn_list" mode="live">
                                    <li class="loading">loading...</li>
                                </ul>
                            </div>
                            <div class="wnd_bottom live" style="display: none;">
                                <div class="r-l"><a class="btn-live-more" href="http://live.bilibili.com/i/following"
                                                    target="_blank">所有关注</a><a class="btn-ignore-all">忽略全部</a></div>
                            </div>
                            <div class="dyn_list_wrapper view-draw" mode="draw" style="display: none;">
                                <ul class="dyn_list" mode="draw">
                                    <li class="loading">loading...</li>
                                </ul>
                            </div>
                            <div class="wnd_bottom">
                                <div class="r-l"><a class="read-more" href="http://www.bilibili.com/account/dynamic">查看全部</a>
                                    <div class="num" id="dynamic_num"></div>
                                    <div class="check-all no-select"></div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <%--收藏夹--%>
                    <li id="i_menu_fav_btn" guest="no" i_menu="#i_menu_fav" class="u-i" style="display: list-item;">
                        <a class="i-link" href="" target="_blank">收藏夹</a>
                        <div class="mini-wnd" style="left: -90.5px; top: 42px; display: none;">
                            <ul class="list">
                                <li><a href="http://www.bilibili.com/video/av3239560" target="_blank"
                                       title="2015新番动画中的相似镜头提纯 · 合">2015新番动画中的相似镜头提纯 · 合</a></li>
                                <li><a href="http://www.bilibili.com/video/av957991" target="_blank"
                                       title="迈克尔·杰克逊：月球漫步 Michael Jackson: Moonwalker">迈克尔·杰克逊：月球漫步 Michael Jackson:
                                    Moonwalker</a></li>
                                <li><a href="http://www.bilibili.com/video/av4249577" target="_blank"
                                       title="【成龙】干物叔！小龙！">【成龙】干物叔！小龙！</a></li>
                                <li><a href="http://www.bilibili.com/video/av2893501" target="_blank"
                                       title="【金坷垃】无名的怪物">【金坷垃】无名的怪物</a></li>
                                <li><a href="http://www.bilibili.com/video/av717241" target="_blank"
                                       title="【第11回MMD杯本選】没有名字的怪物【结月缘】">【第11回MMD杯本選】没有名字的怪物【结月缘】</a></li>
                                <li><a href="http://www.bilibili.com/video/av615027" target="_blank"
                                       title="【MMD教程】PMX編集表情向教程">【MMD教程】PMX編集表情向教程</a></li>
                            </ul>
                            <a class="read-more" href="http://space.bilibili.com/#!/favlist" target="_blank">查看更多<i
                                    class="b-icon b-icon-arrow-r"></i></a></div>
                    </li>
                    <%--历史--%>
                    <li id="i_menu_history_btn" guest="no" class="u-i" style="display: list-item;">
                        <a class="i-link" href="">历史</a>
                        <div class="mini-wnd" style="left: -97px; top: 42px; display: none;">
                            <ul class="list history">
                                <li class="timeline"><span class="date">今日</span></li>
                                <li><a href="http://www.bilibili.com/video/av988140" target="_blank"
                                       title="【MMD剧场】作死就会死，弟弟你咋就不明白">【MMD剧场】作死就会死，弟弟你咋就不明白</a></li>
                                <li><a href="http://www.bilibili.com/video/av4424267" target="_blank"
                                       title="DICE1：清洁工挑战">DICE1：清洁工挑战</a></li>
                                <li class="timeline"><span class="date">昨日</span></li>
                                <li><a href="http://www.bilibili.com/video/av4213295" target="_blank"
                                       title="初音和迪迦的疯狂青蛙">初音和迪迦的疯狂青蛙</a></li>
                                <li><a href="http://www.bilibili.com/video/av4414161" target="_blank"
                                       title="【海王星MAD】Dimension tripper!!!!【short ver.】">【海王星MAD】Dimension
                                    tripper!!!!【short ver.】</a></li>
                                <li><a href="http://www.bilibili.com/video/av4409403" target="_blank"
                                       title="【合集】绝园的暴风雨">【合集】绝园的暴风雨</a></li>
                                <li><a href="http://www.bilibili.com/video/av4352993" target="_blank"
                                       title="【广东药科大学13医用】openshift搭建java web网站教程">【广东药科大学13医用】openshift搭建java
                                    web网站教程</a></li>
                            </ul>
                            <a class="read-more" href="http://www.bilibili.com/account/history" target="_blank">查看更多<i
                                    class="b-icon b-icon-arrow-r"></i></a></div>
                    </li>

                    <li id="i_menu_login_reg" guest="yes" class="u-i" style="display: none;">
                        <a id="i_menu_login_btn" class="i-link login"
                           href=""><span>登录</span></a><i class="s-line"></i><a
                            id="i_menu_register_btn" class="i-link reg" href=""><span>注册</span></a>
                    </li>
                    <li class="u-i b-post">
                        <a class="i-link" href=""
                           target="_blank">签 到</a>
                    </li>
                    <li class="u-i b-post" style="margin-left: 5px">
                        <a class="i-link" href=""
                           target="_blank">关 灯</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
