<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="include/top_container.jsp" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%--阅读漫画页面--%>

<style>
    /*指定阅读页面的背景*/
    body {
        background-color: aquamarine;
    }

    .pagetip {
        display: block;
    }
</style>
<%--图片展示页面--%>
<div style="text-align: center">
    <span id="title"></span>
    <ul id="mainview" style="text-align: center">

    </ul>
</div>

<%--创建页面的代码--%>
<script type="text/javascript">
    $(document).ready(function () {
        var thisChapterId =${requestScope.thisChapter.id};
        var comicId =${requestScope.thisChapter.comic_id};
        var mainview = $('#mainview');
        //
        var imagesJson = '${requestScope.images}';
        var chaptersJson = '${requestScope.chapters}';
        //图片对象数组，内有图片地址
        var images = $.parseJSON(imagesJson);
        var chapters = $.parseJSON(chaptersJson);

        //遍历图片
        $.each(images, function (n, value) {
            //n是index，value是每一个对象
            //console.log(n+":"+value.address);
            var eachImagge = $("<li><img src='" + value.address + "'/></li>");
            var pagehint = $('<span class="pagetip">' + (n + 1) + '/' + images.length + '</span>');
            eachImagge.append(pagehint);
            mainview.append(eachImagge);
        });

        //按钮
        $.each(chapters, function (n, value) {
            if (value.id == thisChapterId) {
                $('#title').text('第' + value.comic_index + '话,' + value.title);
            } else if (value.id < thisChapterId) {
                //上一章
                var preBtn = $("<a href=''>上一话</a>");
                preBtn.attr("href", "/read?chapterid=" + value.id);
                preBtn.text('上一话,' + value.title);
                mainview.prepend(preBtn);
            } else {
                //下一章
                var nextBtn = $("<a href=''>下一话</a>");
                nextBtn.attr("href", "/read?chapterid=" + value.id);
                nextBtn.text('下一话,' + value.title);
                mainview.append(nextBtn);
            }

        });

        historyMoudle.addHistory(comicId, thisChapterId);
    });
</script>

</body>
</html>
<%--测试的url--%>
<%--http://localhost:8080/read?chapterid=479483#--%>
